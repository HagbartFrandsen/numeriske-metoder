#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void printm(gsl_matrix *A);
void printv(gsl_vector *b);
double dot_product(gsl_vector* a, gsl_vector* b);

void matrix_column_get(gsl_matrix* A, gsl_vector* b, int i);

void matrix_column_get(gsl_matrix* A, gsl_vector* b, int i);
void matrix_column_set(gsl_matrix* A, gsl_vector* b, int i);

void qr_gs_solve(gsl_matrix* Q,gsl_matrix* R, gsl_vector* b);

void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);

void givens_qr(gsl_matrix* A);
void givens_qr_QTvec(gsl_matrix* QR, gsl_vector* v);
void givens_qr_solve(gsl_matrix* QR, gsl_vector* b);
void givens_qr_unpack_Q(gsl_matrix* QR, gsl_matrix* Q);
void givens_qr_unpack_R(const gsl_matrix* QR, gsl_matrix* R);

void qr_solve_inplace(gsl_matrix* QR, gsl_vector* b);
void qr_invert(const gsl_matrix* QR, gsl_matrix* Ainverse);

void newton_with_jacobian(void f(gsl_vector* x,gsl_vector* fx), void jacobian(gsl_vector* x, gsl_matrix* J), gsl_vector* x, double dx, double eps);

int newton_with_Jacobian_refined(void f(gsl_vector* x, gsl_vector* fx),void Jmatrix(gsl_vector* x, gsl_matrix* J), gsl_vector* xstart, double dx, double epsilon);

gsl_vector* gradient(double f(gsl_vector* x), gsl_vector* x, double dx);
int qnewton(double f(gsl_vector* x), gsl_vector* p, double dx, double eps);

int newtonMinimization(double f(gsl_vector* x), void gradient(gsl_vector *x, gsl_vector* df), void hessian(gsl_vector* x,gsl_matrix* H), gsl_vector* xstart, double eps);

int downhill_simplex(double F(double*), double** simplex, int d, double simplex_size_goal);
