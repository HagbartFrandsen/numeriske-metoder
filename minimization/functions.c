#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include<math.h>

void printv(gsl_vector *b){
	for(int r=0;r<b->size;r++){
		printf("%.3g\t",gsl_vector_get(b,r));
		printf("\n");
	}
}

void givens_qr(gsl_matrix* A){
	for(int q=0;q<A->size2;q++){
		for(int p=q+1;p<A->size1;p++){
			double theta=atan2(gsl_matrix_get(A,p,q),gsl_matrix_get(A,q,q));
			for(int k=q;k<A->size2;k++){
				double xq=gsl_matrix_get(A,q,k);
				double xp=gsl_matrix_get(A,p,k);
				gsl_matrix_set(A,q,k,xq*cos(theta)+xp*sin(theta));
				gsl_matrix_set(A,p,k,-xq*sin(theta)+xp*cos(theta));
			}
			gsl_matrix_set(A,p,q,theta); 
		}
	}
}

void givens_qr_QTvec(gsl_matrix* QR, gsl_vector* v){
	for(int q=0; q<QR->size2; q++){
		for(int p=q+1;p<QR->size1;p++){
			double theta=gsl_matrix_get(QR,p,q);
			double vq=gsl_vector_get(v,q);
			double vp=gsl_vector_get(v,p);
			gsl_vector_set(v,q,vq*cos(theta)+vp*sin(theta));
			gsl_vector_set(v,p,-vq*sin(theta)+vp*cos(theta));
		}
	}
}

void givens_qr_solve(gsl_matrix* QR, gsl_vector* b, gsl_vector* x){
	givens_qr_QTvec(QR,b); 
	for (int i=QR->size2-1; i>=0; i--){ //back-substitution
		double s=0;
		for(int k=i+1; k<QR->size2; k++) s+=gsl_matrix_get(QR,i,k)*gsl_vector_get(x,k);
		gsl_vector_set(x,i,(gsl_vector_get(b,i)-s)/gsl_matrix_get(QR,i,i));
	}
}

int newtonMinimization(double f(gsl_vector* x), void gradient(gsl_vector *x, gsl_vector* df), void hessian(gsl_vector* x,gsl_matrix* H), gsl_vector* xstart, double eps){
	int n=xstart->size;
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* y  = gsl_vector_alloc(n);
	gsl_matrix* H = gsl_matrix_alloc(n,n);
	gsl_vector* HDx = gsl_vector_alloc(n);
	double fx, Dxd;
	gsl_vector* minF = gsl_vector_alloc(n);
	gsl_vector* dfx = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	double dx = 0.001;
	int steps = 0;
	do{

		steps++;
		fx = f(xstart);
		gradient(xstart,dfx);
		hessian(xstart,H);

		givens_qr(H);
		gsl_vector_scale(dfx,-1.0);
		givens_qr_solve(H,dfx,Dx);
		gsl_vector_scale(dfx,-1.0);
gsl_blas_dgemv(CblasNoTrans,1.0,H,Dx,0.0,HDx);
		for (int i = 0; i < n; i++){
			gsl_vector_set(minF,i,gsl_vector_get(dfx,i)+gsl_vector_get(HDx,i));
		}
		double lambda=2;
		do{
			lambda/=2;
			gsl_vector_memcpy(y,Dx);
			gsl_vector_scale(y,lambda);
			gsl_vector_add(y,xstart);
			Dxd = 0;
			for (int i = 0; i < n; ++i){
				Dxd+=gsl_vector_get(Dx,i)*gsl_vector_get(dfx,i);
			}
		}while(fabs(f(y)) > fabs(fx) + 0.0001*lambda*(Dxd)  && lambda>0.02);
		gsl_vector_memcpy(xstart,y);	
	}while(gsl_blas_dnrm2(Dx)>dx && gsl_blas_dnrm2(dfx)>eps);

	gsl_vector_free(Dx);
	gsl_vector_free(HDx);
	gsl_vector_free(y);
	gsl_vector_free(df);
	gsl_vector_free(dfx);
return steps;
}

double vector_norm(gsl_vector* x){
	int n=x->size;
	double sum=0;
	for(int i=0;i<n;i++){
		sum+=pow(gsl_vector_get(x,i),2);
	}
	return pow(sum,0.5);
}

void element_funktion(int n, int m, gsl_matrix* OUT, gsl_matrix* L, gsl_matrix* R){
double sum=0;
for(int i=0;i<L->size2;i++){
sum+=gsl_matrix_get(L,n,i)*gsl_matrix_get(R,i,m); //sum+=L(n,i)*R(i,m)
}
gsl_matrix_set(OUT,n,m,sum);
}

void matrix_multiplication(gsl_matrix* OUT, gsl_matrix* L, gsl_matrix* R){

int m=OUT->size2, n=OUT->size1;

for(int j=0;j<m;j++){
for(int i=0;i<n;i++){
element_funktion(i,j,OUT,L,R);
}
}
}

void gradient(double f(gsl_vector* x), gsl_vector* x, double dx,gsl_vector* grad_f){
	double fx=f(x);
	for (int i=0; i<x->size; i++){
		gsl_vector_set(x,i,gsl_vector_get(x,i)+dx);
		gsl_vector_set(grad_f,i,(f(x)-fx)/dx);
		gsl_vector_set(x,i,gsl_vector_get(x,i)-dx);
	}
}

void matrix_vector_prod(gsl_matrix* A, gsl_vector* b, gsl_vector* out){
	gsl_vector* c = gsl_vector_alloc(A->size1);
	for (int i = 0; i < A->size1; i++){
		double P=0;
		for(int j=0;j < b->size; j++){
			P+=gsl_matrix_get(A,i,j)*gsl_vector_get(b,j);
		}
		gsl_vector_set(c,i,P);
	}
	for (int i = 0; i < c->size; ++i){
		gsl_vector_set(out,i,gsl_vector_get(c,i));
	}
}

int qnewton(double f(gsl_vector* x), gsl_vector* x, double dx, double eps){
	
	int n=x->size;	
	gsl_vector* y = gsl_vector_alloc(n);
	gsl_vector* s = gsl_vector_alloc(n);
	gsl_vector* z = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	gsl_vector* grad_f = gsl_vector_alloc(n);
	gsl_vector* grad_fy = gsl_vector_alloc(n);
	gsl_matrix* H1 = gsl_matrix_alloc(n,n);

	double fx, fy, vector_dot;
	
	gsl_vector* H1y = gsl_vector_alloc(n);	
	gsl_vector* u = gsl_vector_alloc(n);
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* dH = gsl_matrix_alloc(n,n);
	gsl_vector* v = gsl_vector_alloc(n);

	gradient(f,x,dx,grad_f);
	gsl_matrix_set_identity(H1);
	fx=f(x);
	int nsteps=0;
	
		do{
		nsteps++;
		
		matrix_vector_prod(H1,grad_f,Dx);
		gsl_vector_scale(Dx,-1);
		gsl_vector_memcpy(s,Dx);
		gsl_vector_scale(s,2);
		
		double alpha=0.001;
		do{
			gsl_vector_scale(s,0.5);
			for (int i=0; i<n; i++) gsl_vector_set(z,i,gsl_vector_get(x,i)+gsl_vector_get(s,i));
			fy=f(z);
			vector_dot=0;
			for(int k=0;k<n;k++){vector_dot+=gsl_vector_get(s,k)*gsl_vector_get(grad_f,k);}
			if( fabs(fy) < fabs(fx) + alpha * vector_dot){break;}
			if(vector_norm(s) < dx) {gsl_matrix_set_identity(H1); break;}
		}while(fabs(fy) > fabs(fx) + alpha * vector_dot);
	
		gradient(f,z,dx,grad_fy);
		for (int i=0; i<n; i++) gsl_vector_set(y,i,gsl_vector_get(grad_fy,i)-gsl_vector_get(grad_f,i));
		
		matrix_vector_prod(H1,y,H1y);
		for (int i=0; i<n; i++) gsl_vector_set(u,i,gsl_vector_get(s,i)-gsl_vector_get(H1y,i));
		for (int i=0; i<n; i++) for (int j=0; j<n; j++) gsl_matrix_set(A,i,j,gsl_vector_get(u,i)*gsl_vector_get(s,j));
		matrix_multiplication(dH,A,H1);
		matrix_vector_prod(H1,s,v);
			vector_dot=0;
			for(int k=0;k<n;k++){vector_dot+=gsl_vector_get(y,k)*gsl_vector_get(v,k);}
		gsl_matrix_scale(dH,1/vector_dot);	
	
		gsl_matrix_add(H1,dH);
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(grad_f,grad_fy);
		fx=fy;
	}while(vector_norm(Dx)>dx && vector_norm(grad_f)>eps);

	gsl_vector_free(y);
	gsl_vector_free(s);
	gsl_vector_free(z);
	gsl_vector_free(Dx);
	gsl_vector_free(grad_f);
	gsl_vector_free(grad_fy);
	gsl_matrix_free(H1);
	
	gsl_vector_free(H1y);
	gsl_vector_free(u);
	gsl_vector_free(v);	
	gsl_matrix_free(A);
	gsl_matrix_free(dH);
	
	return nsteps;
}

/* downhill-simplex */
void reflection( double* highest, double* centroid, int dim, double* reflected){
	for(int i=0;i<dim;i++) reflected[i]=2*centroid[i]-highest[i];
}

void expansion( double* highest, double* centroid, int dim, double* expanded){
	for(int i=0;i<dim;i++) expanded[i]=3*centroid[i]-2*highest[i];
}

void contraction( double* highest, double* centroid, int dim, double* contracted){
	for(int i=0;i<dim;i++) contracted[i]=0.5*centroid[i]+0.5*highest[i];
}

void reduction( double** simplex, int dim, int lo){
	for(int k=0;k<dim+1;k++) if(k!=lo)
		for(int i=0;i<dim;i++)
			simplex[k][i]=0.5*(simplex[k][i]+simplex[lo][i]);
}

double distance(double* a, double* b, int dim){
	double s=0;
	for(int i=0;i<dim;i++) s+=pow(a[i]-b[i],2);
	return sqrt(s);
}

double size(double** simplex,int dim){
	double s=0;
	for(int k=1;k<dim+1;k++){
		double dist=distance(simplex[0],simplex[k],dim);
		if(dist>s) s=dist;
		}
	return s;
}

void simplex_update(
		double** simplex, double* f_values, int d,
		int* hi, int* lo, double* centroid){

	*hi=0; double highest=f_values[0];
	*lo=0; double lowest =f_values[0];

	for(int k=1;k<d+1;k++){
		double next=f_values[k];
		if(next>highest){highest=next;*hi=k;}
		if(next<lowest){lowest=next;*lo=k;}
		}

	for(int i=0;i<d;i++){
		double sum=0;
		for(int k=0;k<d+1;k++) if(k!=*hi) sum+=simplex[k][i];
		centroid[i]=sum/d;
		}
}

void simplex_initiate(
			double (*fun)(double*),
			double** simplex, double* f_values, int d,
			int* hi, int* lo, double* centroid){

	for(int k=0;k<d+1;k++) f_values[k]=fun(simplex[k]);

	simplex_update(simplex,f_values,d,hi,lo,centroid);

}

int downhill_simplex(
	double F(double*), double** simplex, int d, double simplex_size_goal)
{
int hi,lo,k=0; double centroid[d], F_value[d+1], p1[d], p2[d];
simplex_initiate(F,simplex,F_value,d,&hi,&lo,centroid);
while(size(simplex,d)>simplex_size_goal){
	simplex_update(simplex,F_value,d,&hi,&lo,centroid);
	reflection(simplex[hi],centroid,d,p1); double f_re=F(p1);
	if(f_re<F_value[lo]){
		expansion(simplex[hi],centroid,d,p2); double f_ex=F(p2);
		if(f_ex<f_re){
			for(int i=0;i<d;i++)simplex[hi][i]=p2[i]; F_value[hi]=f_ex;}
		else{
			for(int i=0;i<d;i++)simplex[hi][i]=p1[i]; F_value[hi]=f_re;}}
	else{
		if(f_re<F_value[hi]){
			for(int i=0;i<d;i++)simplex[hi][i]=p1[i]; F_value[hi]=f_re; }
		else{
			contraction(simplex[hi],centroid,d,p1); double f_co=F(p1);
			if(f_co<F_value[hi]){
				for(int i=0;i<d;i++)simplex[hi][i]=p1[i]; F_value[hi]=f_co; }
			else {
				reduction(simplex,d,lo);
				simplex_initiate(F,simplex,F_value,d,&hi,&lo,centroid); }}}
	k++;} return k;
}
