#include<stdio.h>
#include<math.h>
#include"functions.h"

double f_exp(double* x){
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	double A = x[0];
	double T = x[1];
	double B = x[2];
	double sum = 0;
	for(int i=0;i<N;i++){
		double f=A*exp(-t[i]/T)+B;
		sum +=  pow(f-y[i],2)/pow(e[i],2);
	}
	return sum;
}

int main(){
	printf("Opgave C:\n");

	double R(double* p){
		double x=p[0];
		double y=p[1];
		double z=(1-x)*(1-x)+100*(y-x*x)*(y-x*x);
		return z;
		};

	double H(double* p){
		double x=p[0];
		double y=p[1];
		double z=pow(x*x+y-11,2)+pow(x+y*y-7,2);
		return z;
		};
	
	int d=2;
	double simplex_size_goal=1e-6;


	double** simplex=(double**)calloc(d+1,sizeof(double*));
	for(int col=0;col<d+1;col++) simplex[col]=(double*)calloc(d,sizeof(double));

	for(int k=0;k<d+1;k++) for(int i=0;i<d;i++)
		simplex[k][i]=5+(k==i? 6 : 0);

	printf("Finding minimum of Rosenbrock function:\n");
	printf("exact minimum: f(1, 1)=0\n");
	printf("initial simplex:\n");
	for(int k=0;k<d+1;k++){
		for(int i=0;i<d;i++) printf(" %g",simplex[k][i]);
		printf("\n");
		}

	downhill_simplex(R,simplex,d,simplex_size_goal);

	printf("final simplex:\n");
	for(int k=0;k<d+1;k++){
		for(int i=0;i<d;i++) printf(" %g",simplex[k][i]);
		printf(" ; function value: %g\n",R(simplex[k]));
		}

	for(int col=0;col<d+1;col++) simplex[col]=(double*)calloc(d,sizeof(double));

	for(int k=0;k<d+1;k++) for(int i=0;i<d;i++)
		simplex[k][i]=5+(k==i? 6 : 0);


	printf("\nFinding minimum of Himmelblau's function:\n");
	printf("exact minimum: f(3, 2)=0\n");
	printf("initial simplex:\n");
	for(int k=0;k<d+1;k++){
		for(int i=0;i<d;i++) printf(" %g",simplex[k][i]);
		printf("\n");
		}

	downhill_simplex(H,simplex,d,simplex_size_goal);

	printf("final simplex:\n");
	for(int k=0;k<d+1;k++){
		for(int i=0;i<d;i++) printf(" %g",simplex[k][i]);
		printf(" ; function value: %g\n",H(simplex[k]));
		}

/* experimental part iv */

d=3;

	printf("\nLeast Square Fit:\n");
	for(int col=0;col<d+1;col++) simplex[col]=(double*)calloc(d,sizeof(double));

	for(int k=0;k<d+1;k++) for(int i=0;i<d;i++)
		simplex[k][i]=5+(k==i? 6 : 0);

printf("initial simplex:\n");
	for(int k=0;k<d+1;k++){
		for(int i=0;i<d;i++) printf(" %g",simplex[k][i]);
		printf("\n");
		}

	downhill_simplex(f_exp,simplex,d,simplex_size_goal);

	printf("final simplex:\n");
	for(int k=0;k<d+1;k++){
		for(int i=0;i<d;i++) printf(" %g",simplex[k][i]);
		printf(" ; function value: %g\n",f_exp(simplex[k]));
		}
printf("Så koefficienterne blev de samme som i B.\n");
printf("A=%g \t T=%g \t B=%g\n",simplex[0][0],simplex[0][1],simplex[0][2]);
printf("Så vores downhill simplex metode virker, hvilket også kan ses ved at sammenligne plot for B og C.\n");

 /* Fit values */
double A=simplex[0][0];
double T=simplex[0][1];
double B=simplex[0][2];
printf("\n\nt\tf(t)\n\n\n");
for(double i=0;i<10;i+=0.05){
	printf("%g \t %g\n",i,A*exp(-i/T)+B);

}

 /* experimental values */
double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
int N = sizeof(t)/sizeof(t[0]);

printf("\n\n");
printf("t\ty\te\n\n\n");
for(int i=0;i<N;i++){	
double tp=t[i];
double yp=y[i];
double ep=e[i];
printf("%g\t%g\t%g\n",tp,yp,ep);
}

return 0;
}
