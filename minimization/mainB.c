#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include"functions.h"


/* hessians */ 

void H_Rosenbrock(gsl_vector* x, gsl_matrix* J){
double x0=gsl_vector_get(x,0), x1=gsl_vector_get(x,1);
double J00,J01,J10,J11;
J00=400*3*pow(x0,2)-400*(x1-0.005);
J01=-400*x0;
J10=-200*2*x0;
J11=200;
gsl_matrix_set(J,0,0,J00);
gsl_matrix_set(J,0,1,J01);
gsl_matrix_set(J,1,0,J10);
gsl_matrix_set(J,1,1,J11);
}

void H_Himmelblau(gsl_vector* x, gsl_matrix* J){
double x0=gsl_vector_get(x,0), x1=gsl_vector_get(x,1);
double J00,J01,J10,J11;
J00=12*pow(x0,2)+4*(x1-11);
J01=4*x1+4*x0;
J10=4*x1+4*x0;
J11=12*pow(x1,2)+4*(x0-7);
gsl_matrix_set(J,0,0,J00);
gsl_matrix_set(J,0,1,J01);
gsl_matrix_set(J,1,0,J10);
gsl_matrix_set(J,1,1,J11);
}



void gradient_Rosenbrock(gsl_vector* x, gsl_vector* dx){
double x0=gsl_vector_get(x,0), x1=gsl_vector_get(x,1);
double dx0=400*pow(x0,3)-400*x0*(x1-0.005)-2;
double dx1=200*x1-200*pow(x0,2);
gsl_vector_set(dx,0,dx0);
gsl_vector_set(dx,1,dx1);
}


void gradient_Himmelblau(gsl_vector* x, gsl_vector* dx){
double x0=gsl_vector_get(x,0), x1=gsl_vector_get(x,1);
double dx0=4*pow(x0,3)+4*x0*(x1-10.5)+2*(pow(x1,2)-7);
double dx1=4*pow(x1,3)+4*x1*(x0-6.5)+2*pow(x0,2)-22;
gsl_vector_set(dx,0,dx0);
gsl_vector_set(dx,1,dx1);
}



double f_exp(gsl_vector* x){
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	double sum = 0;
	for(int i=0;i<N;i++){
		double f=A*exp(-t[i]/T)+B;
		sum +=  pow(f-y[i],2)/pow(e[i],2);
	}
	return sum;
}


int main(){
	printf("Opgave B:\n");

	int ncalls=0;

	double R(gsl_vector* p){
		ncalls++;
		double x=gsl_vector_get(p,0);
		double y=gsl_vector_get(p,1);
		double z=(1-x)*(1-x)+100*(y-x*x)*(y-x*x);
		return z;
		};

	double H(gsl_vector* p){
		ncalls++;
		double x=gsl_vector_get(p,0);
		double y=gsl_vector_get(p,1);
		double z=pow(x*x+y-11,2)+pow(x+y*y-7,2);
		return z;
		};






int n=2;
gsl_vector* xstart=gsl_vector_alloc(n);
	gsl_vector_set(xstart,0,2);
	gsl_vector_set(xstart,1, 3);
	int nsteps;
double eps=1e-8;
double dx=1e-8;

	printf("Finding minimum of Rosenbrock function:\n");
	printf("initial vector v:\n");
	printv(xstart);
	printf("objective_function(v) = %g \n",R(xstart));
	printf("calling qnewton...\n");
ncalls=0; nsteps=qnewton(R, xstart,dx,eps);
	printf("nsteps =%i\n",nsteps);
	printf("ncalls =%i\n",ncalls);
	printf("solution x:\n");
	printv(xstart);
	printf("objective_function(x) = %g\n",R(xstart));


gsl_vector_set(xstart,0,-1);
gsl_vector_set(xstart,0,8);
	gsl_vector_set(xstart,0,4);
	gsl_vector_set(xstart,1,3);
	printf("\nFinding minimum of Himmelblau's function:\n");
	printf("initial vector v:\n");
	printv(xstart);
	printf("objective_function(v) = %g \n",H(xstart));
	printf("calling qnewton...\n");
ncalls=0; nsteps=qnewton(H, xstart,dx,eps);
	printf("nsteps =%i\n",nsteps);
	printf("ncalls =%i\n",ncalls);
	printf("solution x:\n");
	printv(xstart);
	printf("objective_function(x) = %g\n",H(xstart));



/* experimental part iv */

gsl_vector* exp_fit_start=gsl_vector_alloc(3);

gsl_vector_set(exp_fit_start,0,1);
gsl_vector_set(exp_fit_start,1,1);
gsl_vector_set(exp_fit_start,2,1);
nsteps=qnewton(f_exp, exp_fit_start, dx,eps);
printf("A=%g \t T=%g \t B=%g \t steps=%i\n",gsl_vector_get(exp_fit_start,0),gsl_vector_get(exp_fit_start,1),gsl_vector_get(exp_fit_start,2),nsteps);


/* Fit values */
double A=gsl_vector_get(exp_fit_start,0);
double T=gsl_vector_get(exp_fit_start,1);
double B=gsl_vector_get(exp_fit_start,2);
printf("\n\nt\tf(t)\n\n\n");
for(double i=0;i<10;i+=0.05){
	printf("%g \t %g\n",i,A*exp(-i/T)+B);

}

/* experimental values */
double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
int N = sizeof(t)/sizeof(t[0]);

printf("\n\n");
printf("t\ty\te\n\n\n");
for(int i=0;i<N;i++){	
double tp=t[i];
double yp=y[i];
double ep=e[i];
printf("%g\t%g\t%g\n",tp,yp,ep);
}

gsl_vector_free(xstart);
gsl_vector_free(exp_fit_start);

return 0;
}
