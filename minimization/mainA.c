#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include"functions.h"

/* hessians */ 

void H_Rosenbrock(gsl_vector* x, gsl_matrix* J){
double x0=gsl_vector_get(x,0), x1=gsl_vector_get(x,1);
double J00,J01,J10,J11;
J00=400*3*pow(x0,2)-400*(x1-0.005);
J01=-400*x0;
J10=-200*2*x0;
J11=200;
gsl_matrix_set(J,0,0,J00);
gsl_matrix_set(J,0,1,J01);
gsl_matrix_set(J,1,0,J10);
gsl_matrix_set(J,1,1,J11);
}

void H_Himmelblau(gsl_vector* x, gsl_matrix* J){
double x0=gsl_vector_get(x,0), x1=gsl_vector_get(x,1);
double J00,J01,J10,J11;
J00=12*pow(x0,2)+4*(x1-11);
J01=4*x1+4*x0;
J10=4*x1+4*x0;
J11=12*pow(x1,2)+4*(x0-7);
gsl_matrix_set(J,0,0,J00);
gsl_matrix_set(J,0,1,J01);
gsl_matrix_set(J,1,0,J10);
gsl_matrix_set(J,1,1,J11);
}

void gradient_Rosenbrock(gsl_vector* x, gsl_vector* dx){
double x0=gsl_vector_get(x,0), x1=gsl_vector_get(x,1);
double dx0=400*pow(x0,3)-400*x0*(x1-0.005)-2;
double dx1=200*x1-200*pow(x0,2);
gsl_vector_set(dx,0,dx0);
gsl_vector_set(dx,1,dx1);
}

void gradient_Himmelblau(gsl_vector* x, gsl_vector* dx){
double x0=gsl_vector_get(x,0), x1=gsl_vector_get(x,1);
double dx0=4*pow(x0,3)+4*x0*(x1-10.5)+2*(pow(x1,2)-7);
double dx1=4*pow(x1,3)+4*x1*(x0-6.5)+2*pow(x0,2)-22;
gsl_vector_set(dx,0,dx0);
gsl_vector_set(dx,1,dx1);
}

int main(){
	printf("Opgave A:\n");

	int ncalls=0;

	double R(gsl_vector* p){
		ncalls++;
		double x=gsl_vector_get(p,0);
		double y=gsl_vector_get(p,1);
		double z=(1-x)*(1-x)+100*(y-x*x)*(y-x*x);
		return z;
		};

	double H(gsl_vector* p){
		ncalls++;
		double x=gsl_vector_get(p,0);
		double y=gsl_vector_get(p,1);
		double z=pow(x*x+y-11,2)+pow(x+y*y-7,2);
		return z;
		};

int n=2;
gsl_vector* xstart=gsl_vector_alloc(n);
	gsl_vector_set(xstart,0,2);
	gsl_vector_set(xstart,1, 3);
	int nsteps;
double eps=1e-5;

	printf("\nFinding minimum of Rosenbrock function:\n");
	printf("initial vector v:\n");
	printv(xstart);
	printf("objective_function(v) = %g \n",R(xstart));
	printf("calling qnewton...\n");
ncalls=0; nsteps=newtonMinimization(R, gradient_Rosenbrock, H_Rosenbrock, xstart,eps);
	printf("nsteps =%i\n",nsteps);
	printf("ncalls =%i\n",ncalls);
	printf("solution x:\n");
	printv(xstart);
	printf("objective_function(x) = %g\n",R(xstart));


gsl_vector_set(xstart,0,-1);
gsl_vector_set(xstart,0,8);
	gsl_vector_set(xstart,0,4);
	gsl_vector_set(xstart,1,3);
	printf("\nFinding minimum of Himmelblau's function:\n");
	printf("initial vector v:\n");
	printv(xstart);
	printf("objective_function(v) = %g \n",H(xstart));
	printf("calling qnewton...\n");
ncalls=0; nsteps=newtonMinimization(H, gradient_Himmelblau, H_Himmelblau, xstart,eps);
	printf("nsteps =%i\n",nsteps);
	printf("ncalls =%i\n",ncalls);
	printf("solution x:\n");
	printv(xstart);
	printf("objective_function(x) = %g\n",H(xstart));

gsl_vector_free(xstart);

return 0;
}

