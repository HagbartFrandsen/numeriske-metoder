#define pi 3.14159265358979323846264338327950288419716939937510
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include"functions.h"

int main(){

printf("Opgave A:\n");
int N;
double exact=pi/8;
int d=3; double a[] ={0,0,0}; double b[]={pi,pi,pi}; // dimensioner satrt og slut punkter
double result,error;
N=1000000;

plainmc(d,a,b,&fun,N,&result,&error);
printf("Tester Monte carlo for sqrt(x*x+y*y+z*z)\n");
printf("N\t= %d\n",N);
printf("resultat\t= %g\n",result);
printf("eksakt\t= %g\n",exact);
printf("estimered fejl\t= %g\n",error);
printf("faktiske fejl\t= %g\n",fabs(result-exact));

exact=1.3932039296856768591842462603255;
plainmc(d,a,b,&fundifficultsingularintegral,N,&result,&error);
printf("Tester Monte carlo for 1/(1-cos(x)*cos(y)*cos(z))/pi/pi/pi\n");
printf("N\t= %d\n",N);
printf("resultat\t= %g\n",result);
printf("eksakt\t= %g\n",exact);
printf("estimered fejl\t= %g\n",error);
printf("faktiske fejl\t= %g\n",fabs(result-exact));

return 0;
}
