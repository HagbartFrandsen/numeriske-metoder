#define pi 3.14159265358979323846264338327950288419716939937510
#include <math.h>
#include <stdio.h>
#include <assert.h>
#include <gsl/gsl_rng.h>
#define RND ((double)rand()/RAND_MAX)

double fun(double x[]){
	double r = sqrt(x[0]*x[0]+x[1]*x[1]+x[2]*x[2]);
	if(r<1) return r;
	else return 0;}

double fundifficultsingularintegral(double x[]){
return 1/(1-cos(x[0])*cos(x[1])*cos(x[2]))/pi/pi/pi;
}

void randomx(int dim, double* a, double* b, double* x){
for(int i=0; i<dim; i++){
	x[i]=a[i]+RND*(b[i]-a[i]);
}
}

void plainmc(int dim, double* a, double* b, double f(double* x), int N, double* result, double* error){
double V=1;
for(int i=0; i<dim; i++){
	V*=b[i]-a[i];
}
double sum=0, sum2=0, fx, x[dim];
for(int i=0; i<N; i++){
	randomx(dim, a, b, x); 
	fx=f(x);
	sum+=fx;
	sum2+=fx*fx;
}
double avr=sum/N, var=sum2/N-avr*avr;
*result=avr*V;
*error=sqrt(var/N)*V;
}

double adapt24 (double f(double),double a,double b,double f2,double f3,double acc,double eps,double *err){ 
double h=b-a,f1,f4;

f1=f(a+h/6);

f4=f(a+5*h/6);
double q4=(f1+f4)*h/3+(f2+f3)*h/6;
double q2=(f1+f2+f3+f4)*h/4;
double tol=acc+eps*fabs(q4); *err=fabs(q4-q2)/3;
if(*err < tol){
	return q4;
}
else{
	double i1,i2,err1,err2;
	i1=adapt24(f,a,(a+b)/2,f1,f2,acc/sqrt(2.),eps,&err1);
	i2=adapt24(f,(a+b)/2,b,f3,f4,acc/sqrt(2.),eps,&err2);
	*err=sqrt(err1*err1+err2*err2);
	return i1+i2;
}
}

double adapt(double f(double),double a,double b,double acc,double eps,double *err){
double f2=f(a+(b-a)/3); double f3=f(a+2*(b-a)/3);
double integ=adapt24(f,a,b,f2,f3,acc,eps,err);
return integ;
}

double adapt_open(double f(double), double a, double b,double f2, double f3, double acc, double eps, double *err, int nrec){
  assert(nrec < 100000);
  double h = b-a;
  double f1 = f(a+h/6);
  double f4 = f(a+5*h/6);
  double Q = (2*f1+f2+f3+2*f4)*h/6;
  double q = (f1+f2+f3+f4)*h/4;
  double tol = acc + eps*fabs(Q);
  *err = fabs(Q-q);
  if(*err < tol) return Q;
  else{
    double err1,err2;
    double Q1 = adapt_open(f,a,(a+b)/2,f1,f2,acc/sqrt(2.0),eps,&err1,nrec++);
    double Q2 = adapt_open(f,(a+b)/2,b,f2,f3,acc/sqrt(2.0),eps,&err2,nrec++);
    *err = sqrt(err1*err1+err2*err2);
    return Q1+Q2;
  }
}

double int_open(double f(double), double a, double b, double acc, double eps, double *err){
  if(isinf(a) && isinf(b)){
    double ft(double t){return f(t/(1-t*t)) * (1+t*t)/pow(1-t*t,2);}
    double a = -1; b=1;
    double f2 = ft(a+(b-a)/3);
    double f3 = ft(a+2*(b-a)/3);
    return adapt_open(ft,a,b,f2,f3,acc,eps,err,0);
  }
  else if(isinf(b)){
    double ft(double t){return f(a+t/(1-t))*1/pow(1-t,2);};
    double a = 0; b=1;
    double f2 = ft(a+(b-a)/3);
    double f3 = ft(a+2*(b-a)/3);
    return adapt_open(ft,a,b,f2,f3,acc,eps,err,0);
  }
  else if(isinf(a)){
    double ft(double t){return f(b+t/(1+t))*1/pow(1+t,2);}
    double a = -1; b=0;
    double f2 = ft(a+(b-a)/3);
    double f3 = ft(a+2*(b-a)/3);
    return adapt_open(ft,a,b,f2,f3,acc,eps,err,0);
  }
  else{
    double f2 = f(a+(b-a)/3);
    double f3 = f(a+2*(b-a)/3);
    return adapt_open(f,a,b,f2,f3,acc,eps,err,0);
  }
    
}

double clenshaw_curtis(double f(double), double a, double b, double acc, double eps,double *err){
  double g(double t){return f((a+b)/2+(a-b)/2*cos(t))*sin(t)*(b-a)/2;}
  return int_open(g,0,M_PI,acc,eps,&err);
}
