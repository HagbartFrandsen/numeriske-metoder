#define pi 3.14159265358979323846264338327950288419716939937510
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include"functions.h"

int main(){

double exact=pi/8;
int d=3; double a[] ={0,0,0}; double b[]={pi,pi,pi}; // dimensioner satrt og slut punkter
double result,error;
printf("N\terror\testimated error\n");
for(int N=1e3; N<1e5; N=N+1e2){
	plainmc(d,a,b,&fun,N,&result,&error);

	printf("%d\t%g\t%g\n",N,error,fabs(result-exact));
}

return 0;
}
