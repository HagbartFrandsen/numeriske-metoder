#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include"functions.h"

int main(){

	printf("Opgave C:\n");
	int N;
	double exact=0.333333;
int callsAdapt=0;
int callsMonte=0;
int callsclenshaw=0;
double fun0(double x){callsAdapt++;return x*x;}
double fun1(double x[]){callsMonte++;return x[0]*x[0];}
double func_openQuad(double x){callsclenshaw++;return x*x;}
double result,error;
N=1000000;

  int dim = 1;
  double a[] = {0};
  double b[] = {1};

result=adapt(fun0,0,1,1e-4,1e-4,&error);
printf("Tester Adaptive integration for X*X\n");
printf("N\t= %d\n",callsAdapt);
printf("resultat\t= %g\n",result);
printf("eksakt\t= %g\n",exact);
printf("estimered fejl\t= %g\n",error);
printf("faktiske fejl\t= %g\n",fabs(result-exact));

plainmc(dim,a,b,&fun1,N,&result,&error);
printf("Tester Monte carlo for X*X\n");
printf("N\t= %d\n",N);
printf("resultat\t= %g\n",result);
printf("eksakt\t= %g\n",exact);
printf("estimered fejl\t= %g\n",error);
printf("faktiske fejl\t= %g\n",fabs(result-exact));

  result = clenshaw_curtis(func_openQuad,0,1,1e-4,1e-4,&error);
printf("Tester clenshaw curtis for X*X\n");
printf("N\t= %d\n",callsclenshaw);
printf("resultat\t= %g\n",result);
printf("eksakt\t= %g\n",exact);
printf("estimered fejl\t= %g\n",error);
printf("faktiske fejl\t= %g\n",fabs(result-exact));

printf("\nSom vi kan se, for en simple ligning som x*x, er adptive integration bedst, efterfulgt af clenshaw curtis og så Monte carlo.\n");

return 0;
}

