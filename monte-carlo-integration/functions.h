void randomx(int dim, double* a, double* b, double* x);
void plainmc(int dim, double* a, double* b, double f(double* x), int N, double* result, double* error);

double fun(double x[]);

double fundifficultsingularintegral(double x[]);

double clenshaw_curtis(double f(double), double a, double b, double acc, double eps,double *err);

double adapt(double f(double),double a,double b,double acc,double eps,double *err);
