#include<stdio.h>
#include<math.h>
#include"functions.h"

int main (void) {
int ncalls=0;
double err, acc=1e-5, eps=acc;
printf("Opgave A med præcition eps=acc= %g :\n",acc);
double p;

double fun1(double x){
	ncalls++;
	return pow(x,p);
}

p=0.5; /* laver fun1 til sqrt(x) */
double q=adapt(fun1,0,1,acc,eps,&err);
printf("integral fra 0 til 1 af sqrt(x) er %g, ved antal kald %i\n",q,ncalls);
p=-0.5; /* laver fun1 til 1/sqrt(x) */
ncalls=0;
q=adapt(fun1,0,1,acc,eps,&err);
printf("integral fra 0 til 1 af 1/sqrt(x) er %g, ved antal kald %i\n",q,ncalls);

double fun2(double x){
	ncalls++;
	return log(x)/sqrt(x);
}
ncalls=0;
q=adapt(fun2,0,1,acc,eps,&err);
printf("integral fra 0 til 1 af log(x)/sqrt(x) er %g, ved antal kald %i\n",q,ncalls);

double fun3(double x){
	ncalls++;
	return 4*sqrt(1-pow(1-x,2));
}
ncalls=0;
q=adapt(fun3,0,1,acc,eps,&err);
printf("integral fra 0 til 1 af 4*sqrt(1-pow(1-x,2) er %g, ved antal kald %i\n",q,ncalls);

return 0;
}
