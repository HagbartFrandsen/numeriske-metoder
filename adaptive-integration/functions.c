#include <math.h>
#include <stdio.h>

double adapt24 (double f(double),double a,double b,double f2,double f3,double acc,double eps,double *err){ 
double h=b-a,f1,f4;

f1=f(a+h/6);

f4=f(a+5*h/6);
double q4=(f1+f4)*h/3+(f2+f3)*h/6;
double q2=(f1+f2+f3+f4)*h/4;
double tol=acc+eps*fabs(q4); *err=fabs(q4-q2)/3;
if(*err < tol){
	return q4;
}
else{
	double i1,i2,err1,err2;
	i1=adapt24(f,a,(a+b)/2,f1,f2,acc/sqrt(2.),eps,&err1);
	i2=adapt24(f,(a+b)/2,b,f3,f4,acc/sqrt(2.),eps,&err2);
	*err=sqrt(err1*err1+err2*err2);
	return i1+i2;
}
}

double adapt(double f(double),double a,double b,double acc,double eps,double *err){
double f2=f(a+(b-a)/3); double f3=f(a+2*(b-a)/3);
double integ=adapt24(f,a,b,f2,f3,acc,eps,err);
return integ;
}

double adaptinf(double f(double),double a,double b,double acc,double eps,double *err){
double integ;
	double fminfinf(double t){ /* funktion fra -inf til inf */
		return f(t/(1-pow(t,2)))*(1+pow(t,2))/pow(1-pow(t,2),2);
	}
	double finf(double t){ /* funktion til inf */
		return f(a+t/(1-t))*1/pow(1-t,2);
	}
	double fminf(double t){ /* funktion fra -inf */
		return f(b-t/(1+t))*(-1)/pow(1+t,2);
	}
if(isinf(a)==-1 && isinf(b)==1){
integ=adapt(fminfinf,-1,1,acc,eps,err);
}
else if(isinf(a)==0 && isinf(b)==1){
integ=adapt(finf,0,1,acc,eps,err);
}
else if(isinf(a)==-1 && isinf(b)==0){
integ=adapt(fminf,-1,0,acc,eps,err);
}
else {
integ=adapt(f,a,b,acc,eps,&err);
}
return integ;
}

void rkstep23( void f(int n,double x,double* y,double* dydx),
int n, double x, double* yx, double h, double* yh, double* dy){
  int i; double k1[n],k2[n],k3[n],k4[n],yt[n]; /* VLA: -std=c99 */
  f(n,   x    ,yx,k1); for(i=0;i<n;i++) yt[i]=yx[i]+1./2*k1[i]*h;
  f(n,x+1./2*h,yt,k2); for(i=0;i<n;i++) yt[i]=yx[i]+3./4*k2[i]*h;
  f(n,x+3./4*h,yt,k3); for(i=0;i<n;i++)
    yh[i]=yx[i]+(2./9 *k1[i]+1./3*k2[i]+4./9*k3[i])*h;
  f(n,  x+h   ,yh,k4);  for(i=0;i<n;i++){
    yt[i]=yx[i]+(7./24*k1[i]+1./4*k2[i]+1./3*k3[i]+1./8*k4[i])*h;
    dy[i]=yh[i]-yt[i];
    }
}

void ode_driver(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max){
int i,k=0; 
double x,*y,s,err,normy,tol,a=xlist[0],yh[n],dy[n];
while(xlist[0]<b){
	x=xlist[0], y=ylist[0]; 
	if(x+h>b){
		h=b-x;
	}
	rkstep23(f,n,x,y,h,yh,dy);
	s=0;
	for(i=0;i<n;i++){
		s+=dy[i]*dy[i]; 
		err=sqrt(s);
	}
	s=0;
	for(i=0;i<n;i++){
		s+=yh[i]*yh[i];
		normy=sqrt(s);
	}
	tol=(normy*eps+acc)*sqrt(h/(b-a));
	if(err<tol){ /* accept step and continue */
	k++;
		if(k>max-1){
			printf("max steps reached in ode_driver\n");
			break;
		}
		xlist[0]=x+h;
		for(i=0;i<n;i++){
			ylist[0][i]=yh[i];
		}
	}
	if(err>0){
		h*=pow(tol/err,0.25)*0.95;
	}
	else{
		h*=2;
	}
} /* end while */

	printf("\n antal kald %i\n",k);

 /* return the number of entries in xlist/ylist */
} 

void f(int n, double x, double* y, double* dydx){/* f(x)=2*x*e^x² */
	dydx[0]=exp(pow(x,2));
	dydx[1]=2*x*exp(pow(x,2));
	return;}
