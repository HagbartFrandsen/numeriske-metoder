#include<stdio.h>
#include<math.h>
#include"functions.h"

int main (void) {
int ncalls=0;
double err, acc=1e-5, eps=acc;
printf("Opgave B med præcition eps=acc= %g :\n",acc);

double fun1(double x){
	ncalls++;
	return exp(-(pow(x,2)+x+1));
}

double q=adaptinf(fun1,-INFINITY,INFINITY,acc,eps,&err);
printf("integral fra -inf til inf af exp(-(pow(x,2)+x+1)) er %g, ved antal kald %i\n",q,ncalls);

double fun2(double x){
	ncalls++;
	return exp(-pow(x,2));
}

q=adaptinf(fun2,0,INFINITY,acc,eps,&err);
printf("integral fra 0 til inf af exp(-pow(x,2)) er %g, ved antal kald %i\n",q,ncalls);

double fun3(double x){
	ncalls++;
	return exp(-(pow(x,2)+pow(x,-2)));
}

q=adaptinf(fun3,-INFINITY,0,acc,eps,&err);
printf("integral fra -inf til 0 af exp(-(pow(x,2)+pow(x,-2))) er %g, ved antal kald %i\n",q,ncalls);

return 0;
}

