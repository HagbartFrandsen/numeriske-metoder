#include <math.h>
#include <stdio.h>

double adapt(double f(double),double a,double b, double eps,double acc,double *err);

double adaptinf(double f(double),double a,double b,double acc,double eps,double *err);

double adapt24(double f(double), double a, double b, double f2, double f3, double acc, double eps, double *err);

void rkstep23( void f(int n,double x,double* y,double* dydx), int n, double x, double* yx, double h, double* yh, double* dy);

void ode_driver(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max);

void f(int n, double x, double* y, double* dydx);
