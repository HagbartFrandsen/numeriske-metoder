#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"functions.h"

int main(){

	printf("Opgave C:\n");
	printf("Printer løsningen til -(1/2)u''-(1/r)u = u\n");

	int n=2;
	int max=10000;
	double*xstart=(double*)calloc(1,sizeof(double));
	double**ystart=(double**)calloc(1,sizeof(double*)); 
	ystart[0]=(double*)calloc(n,sizeof(double)); /* putter array ind i hver indgang af arrayet, for at skabe en "matrix" */
	double a=0, b=2, h=0.1, acc=1e-05, eps=acc, err;
	xstart[0]=a; ystart[0][0]=0; ystart[0][1]=1;
	ode_driver(f,n,xstart,ystart,b,h,acc,eps,max);

	printf("For b=%g får vi Q=y(b)=%g\n",b,ystart[0][0]);
	printf("Vi ser at vi finder løsniingen for endepunktet b=%g, så vi kan finde løsningen ved brug af ODE funktionen.\n",b);

double fexp(double x){
	return exp(pow(x,2));
}

double q=adaptinf(fexp,a,b,acc,eps,&err);
printf("Integral fra %g til %g af exp(pow(x,2)) er %g, ved antal kald %i, med vores adaptive-integral funktion.\n",a,b,q,n);

printf("Vores adaptive-integral funktion er altså overlegen.\n");

return 0;
}

