#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>

double dot_product(gsl_matrix* A,int i,int j){
double dot=0;
int length=A->size1;
for(int k=0;k<length;k++){
	dot+=gsl_matrix_get(A,k,i)*gsl_matrix_get(A,k,j);
}
return dot;
}

void matrix_column_get(gsl_matrix* A, gsl_vector* b, int i){
for(int j=0;j<A->size1;j++){
	gsl_vector_set(b,j,gsl_matrix_get(A,j,i));
}
}

void matrix_column_set(gsl_matrix* A, gsl_vector* b, int i){
for(int j=0;j<A->size1;j++){
	gsl_matrix_set(A,j,i,gsl_vector_get(b,j));
}
}

/* lånt */

void printm(gsl_matrix *A){
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf("%.3g\t",gsl_matrix_get(A,r,c));
		printf("\n");
	}
}

void printv(gsl_vector *b){
	for(int r=0;r<b->size;r++){
		printf("%.3g\t",gsl_vector_get(b,r));
		printf("\n");
	}
}
/* kig efter senere */

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
int m=A->size2; /* antal kolonner m */
gsl_vector *ai=gsl_vector_alloc(A->size1); /* frigør plads til 2 vektorer, med samme antal indgange som antal rækker i A rækker */
gsl_vector *aj=gsl_vector_alloc(A->size1);

for(int i=0;i<m;i++){
	gsl_matrix_set(R,i,i,pow(dot_product(A,i,i),0.5)); /* prikproduktet laver værdierne for Rii */
	matrix_column_get(A, ai,i); /* Her findes værdier til den i'te søje fra vektoren ai. */
	gsl_vector_scale(ai,1.0/gsl_matrix_get(R,i,i)); /* ganger med 1/R_ii */
	matrix_column_set(A,ai,i); /* de nye værdier rettes nu for A */

	for(int j=i+1;j<m;j++){
		gsl_matrix_set(R,i,j,dot_product(A,i,j)); /* prikproduktet laver værdierne for Rij */
		matrix_column_get(A,aj,j); /* Her findes værdier til den j'te søje fra vektoren aj. */

		for(int k=0;k<aj->size;k++){
			gsl_vector_set(aj,k,gsl_vector_get(aj,k)-gsl_vector_get(ai,k)*gsl_matrix_get(R,i,j)); /* aj=aj-ai*R_ij */
		}
		matrix_column_set(A,aj,j); // Og det sættes ind i matrix A på søjle j.
	}
}
gsl_vector_free(ai);
gsl_vector_free(aj);
}


void qr_gs_solve(gsl_matrix* Q,gsl_matrix* R, gsl_vector* b){
int n=Q->size1, m=Q->size2; /* finder Q^T */
gsl_matrix* QT = gsl_matrix_alloc(m,n);
gsl_matrix_transpose_memcpy(QT,Q);

gsl_vector* QTb = gsl_vector_alloc(m);
gsl_blas_dgemv (CblasNoTrans, 1.0, QT, b, 0.0, QTb); /* udregner Q^Tb */

b->size=m;
// Back-substitution:
for(int i=QTb->size-1; i>=0; i--){
	double s=gsl_vector_get(QTb,i);
	for(int k=i+1;k<QTb->size;k++){ s-=gsl_matrix_get(R,i,k)*gsl_vector_get(b,k);}
		gsl_vector_set(b,i,s/gsl_matrix_get(R,i,i));
	}
gsl_matrix_free(QT);
gsl_vector_free(QTb);
}

void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B){
int n=Q->size1;
int m=Q->size2;
gsl_vector* ei=gsl_vector_alloc(n); /* frigøt plads for egenvektorer */

for(int i=0;i<m;i++){
	ei=gsl_vector_calloc(n); 
	gsl_vector_set(ei,i,1); // element i sættes til 1.
	qr_gs_solve(Q,R,ei); // Udregnes og resultatet indsættes i vektor e_i.
	matrix_column_set(B,ei,i); // e_i indsættes i B på række i.
}
gsl_vector_free(ei);
}

void givens_qr(gsl_matrix* A){
for(int q=0; q<A->size2; q++){
	for(int p=q+1; p<A->size1; p++){
		double theta=atan2(gsl_matrix_get(A, p, q), gsl_matrix_get(A, q, q));
		for(int k=q; k<A->size2; k++){
			double xq=gsl_matrix_get(A, q, k), xp=gsl_matrix_get(A, p, k);
			gsl_matrix_set(A, q, k, xq*cos(theta)+xp*sin(theta));
			gsl_matrix_set(A, p, k,-xq*sin(theta)+xp*cos(theta));
		}
		gsl_matrix_set(A, p, q, theta);
	}
}
}

void givens_qr_QTvec(gsl_matrix* QR, gsl_vector* v){
assert(QR->size1 == v->size);
for(int q=0; q<QR->size2; q++){
	for(int p=q+1; p<QR->size1; p++){
		double theta=gsl_matrix_get(QR, p, q);
		double vq=gsl_vector_get(v, q), vp=gsl_vector_get(v , p);
		gsl_vector_set(v, q, vq*cos(theta)+vp*sin(theta));
		gsl_vector_set(v, p, -vq*sin(theta)+vp*cos(theta));
	}
}
}

void backsub(gsl_matrix* U, gsl_vector* c){
for(int i=c->size-1; i>=0; i--){
	double s=gsl_vector_get(c, i);
	for(int k=i+1; k<c->size; k++){
		s-=gsl_matrix_get(U, i, k)*gsl_vector_get(c, k);
		gsl_vector_set(c, i, s/gsl_matrix_get(U, i, i));
	}
}
}

void givens_qr_solve(gsl_matrix* QR, gsl_vector* b){
givens_qr_QTvec(QR, b);
backsub(QR, b);
}

void givens_qr_unpack_Q(gsl_matrix* QR, gsl_matrix* Q){
gsl_vector* ei=gsl_vector_alloc(QR->size1);
for(int i=0; i<QR->size1; i++){
	gsl_vector_set_basis(ei, i);
	givens_qr_QTvec(QR, ei);
	for(int j=0; j<QR->size2; j++){
		gsl_matrix_set(Q, i, j, gsl_vector_get(ei, j));
	}
}
gsl_vector_free(ei);
}

void givens_qr_unpack_R(const gsl_matrix* QR, gsl_matrix* R){
assert(R->size1==R->size2);
assert(QR->size2==R->size2);
for(int c=0; c<R->size2; c++){ 
	for(int r=0;r<R->size2; r++){
		if(r<=c) gsl_matrix_set(R,r,c,gsl_matrix_get(QR,r,c));
		else gsl_matrix_set(R,r,c,0);
	}
}
}


void qr_solve_inplace(gsl_matrix* QR, gsl_vector* b){
	assert(QR->size1==b->size);
	givens_qr_QTvec(QR, b);
	for(int i=QR->size2-1; i>=0; i--){
		double s=0;
		for(int k=i+1;k<QR->size2;k++)s+=gsl_matrix_get(QR,i,k)*gsl_vector_get(b,k);
		gsl_vector_set(b,i,(gsl_vector_get(b,i)-s)/gsl_matrix_get(QR,i,i));
	}
}

void qr_invert(const gsl_matrix* QR, gsl_matrix* Ainverse){
	gsl_matrix_set_identity(Ainverse);
	for(int i=0; i<QR->size2; i++){
		gsl_vector_view ei = gsl_matrix_column(Ainverse,i);
		qr_solve_inplace(QR,&ei.vector);
	}
}

