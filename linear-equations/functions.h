#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);
void printm(gsl_matrix *A);
void printv(gsl_vector *b);
double dot_product(gsl_matrix* A,int i,int j);

void matrix_column_get(gsl_matrix* A, gsl_vector* b, int i);

void matrix_column_get(gsl_matrix* A, gsl_vector* b, int i);
void matrix_column_set(gsl_matrix* A, gsl_vector* b, int i);

void qr_gs_solve(gsl_matrix* Q,gsl_matrix* R, gsl_vector* b);

void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);

void givens_qr(gsl_matrix* A);
void givens_qr_QTvec(gsl_matrix* QR, gsl_vector* v);
void givens_qr_solve(gsl_matrix* QR, gsl_vector* b);
void givens_qr_unpack_Q(gsl_matrix* QR, gsl_matrix* Q);
void givens_qr_unpack_R(const gsl_matrix* QR, gsl_matrix* R);

void qr_solve_inplace(gsl_matrix* QR, gsl_vector* b);
void qr_invert(const gsl_matrix* QR, gsl_matrix* Ainverse);
