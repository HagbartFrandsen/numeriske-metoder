#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include"functions.h"
#include<math.h>

int main(){
int n=3;
gsl_matrix* A = gsl_matrix_alloc(n,n);
for(int i=0;i<n;i++){
	for(int j=0;j<n;j++){
		gsl_matrix_set(A,i,j,((double)rand()/RAND_MAX));
	}
}
printf("Opgave C\n Printer først A:\n");
printm(A);

printf("Laver så QR decomponition med Givens rotations: \n");

gsl_matrix* Q = gsl_matrix_alloc(n,n);
gsl_matrix_memcpy(Q,A); /* sætter Q=A */
gsl_matrix* R = gsl_matrix_alloc(n,n);

givens_qr(A);

givens_qr_unpack_Q(A,Q);
givens_qr_unpack_R(A,R);

printf("printer Q:\n");
printm(Q);
printf("printer R:\n");
printm(R);

gsl_matrix* QR = gsl_matrix_alloc(n,n);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,Q,R,0.0,QR);
printf("printer QR, for og se om den er lig A:\n");
printm(QR);

gsl_matrix* qtq = gsl_matrix_alloc(n,n);
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,Q,Q,0,qtq);
printf("printer så Q^T*Q for at checke om der er en identitetsmatrix:\n");
printm(qtq);



gsl_vector* b = gsl_vector_alloc(n);
gsl_vector* x = gsl_vector_alloc(n);
for(int i=0;i<n;i++)gsl_vector_set(b,i,((double)rand()/RAND_MAX));
printf("printer b:\n");
printv(b);
gsl_vector_memcpy(x,b);

/* køre solve funktionen */
qr_solve_inplace(A, x);

printf("printer x:\n");
printv(x);

gsl_vector* v = gsl_vector_alloc(n);

printf("Tester om x er rigtig ved at tjekke om QRx=b:\n");
gsl_blas_dgemv(CblasNoTrans,1,QR,x,0,v);
printv(v);

printf("printer A=QR:\n");
printm(QR);
printf("Laver nu den inverse A^I matrix og printer den:\n");
gsl_matrix* AI = gsl_matrix_alloc(n,n);
qr_invert(A, AI); /* A er QR-decomposed på dette tidspunkt! */
printm(AI);
printf("Tester om A^I er rigtig ved at tjekke om A^IA=I:\n");
gsl_matrix* I = gsl_matrix_alloc(n,n);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,QR,AI,0,I);
printm(I);

gsl_matrix_free(QR);
gsl_matrix_free(qtq);
gsl_matrix_free(A);
gsl_matrix_free(Q);
gsl_matrix_free(R);
gsl_matrix_free(AI);
gsl_matrix_free(I);
gsl_vector_free(b);
gsl_vector_free(x);
gsl_vector_free(v);

return 0;
}
