#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include"functions.h"
#include<math.h>





int main(){

size_t n=4,m=3;
gsl_matrix* A = gsl_matrix_alloc(n,m);
gsl_matrix *R = gsl_matrix_alloc(m,m);
for(int i=0;i<n;i++){
	for(int j=0;j<m;j++){
		gsl_matrix_set(A,i,j,((double)rand()/RAND_MAX));
	}
}
printf("Opgave A\n Printer først A:\n");
printm(A);

printf("Laver så QR decomponition og printer Q: \n");
gsl_matrix* Q=A;
qr_gs_decomp(Q,R);
printm(Q);
printf("Printer R:\n");
printm(R);

printf("Transponere Q for at kunne chekke Q^TQ=1: \n");
gsl_matrix* QT = gsl_matrix_alloc(m,n); 
gsl_matrix_transpose_memcpy(QT,Q); /* laver QT som er A transponeret */
printm(QT);

gsl_matrix* QTQ = gsl_matrix_calloc(m,m); /* laver QTQ som en nul matrix */
gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, QT, Q, 0.0, QTQ); /* udregner Q^TQ=QTQ hvor Q=A*/

printf("Matrix Q^TQ printes:\n");
printm(QTQ);
printf("som er en identitetsmatrice\n\n");

gsl_matrix* QR = gsl_matrix_calloc(n,m); /* laver QR som en nul matrix */
gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, Q, R, 0.0, QR); /* udregner QR */

printf("Matrix QR=A printes:\n");
printm(QR);
printf("Og vi ser at den er lig A:\n");

printf("For A.2 hele smøren køres igennem igen:\n");

n=4;
gsl_matrix* A2 = gsl_matrix_alloc(n,n);
gsl_matrix *R2 = gsl_matrix_alloc(n,n);
for(int i=0;i<n;i++){
	for(int j=0;j<n;j++){
		gsl_matrix_set(A2,i,j,((double)rand()/RAND_MAX));
	}
}
printf("Opgave A.2\n Printer først A2:\n");
printm(A2);

printf("Laver så QR decomponition og printer Q: \n");
gsl_matrix* Q2=A2;
qr_gs_decomp(Q2,R2);
printm(Q2);
printf("Printer R2:\n");
printm(R2);

printf("Transponere Q2 for at kunne chekke Q2^TQ2=1: \n");
gsl_matrix* Q2T = gsl_matrix_alloc(n,n); 
gsl_matrix_transpose_memcpy(Q2T,Q2); 
printm(Q2T);

gsl_matrix* Q2TQ2 = gsl_matrix_calloc(n,n);
gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, Q2T, Q2, 0.0, Q2TQ2); 

printf("Matrix Q2^TQ2 printes:\n");
printm(Q2TQ2);
printf("som er en identitetsmatrice\n\n");

gsl_matrix* Q2R2 = gsl_matrix_calloc(n,n);
gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, Q2, R2, 0.0, Q2R2);

printf("Matrix Q2R2=A2 printes:\n");
printm(Q2R2);
printf("Og vi ser at den er lig A2:\n");

gsl_vector* b=gsl_vector_alloc(n);
for(int i=0;i<n;i++){
	gsl_vector_set(b,i,(double)rand()/(double)RAND_MAX);
}
printf("med vector b:\n");
printv(b);
gsl_vector* x=b;
qr_gs_solve(Q2,R2,x);
printf("som giver x:\n");
printv(x);

gsl_vector* bcheck = gsl_vector_alloc(n);
gsl_blas_dgemv (CblasNoTrans, 1.0, Q2R2, x, 0.0, bcheck); /* udregner Q^Tb */
printf("og vi checker om Ax=b:\n");
printv(bcheck);

gsl_matrix_free(A);
gsl_matrix_free(R);
gsl_matrix_free(QT);
gsl_matrix_free(QTQ);
gsl_matrix_free(QR);
gsl_matrix_free(A2);
gsl_matrix_free(R2);
gsl_matrix_free(Q2T);
gsl_matrix_free(Q2TQ2);
gsl_matrix_free(Q2R2);
gsl_vector_free(b);
gsl_vector_free(bcheck);

return 0;
}
