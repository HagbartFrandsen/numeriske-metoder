#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include"functions.h"
#include<math.h>

int main(){
int n=3;
gsl_matrix* A = gsl_matrix_alloc(n,n);
gsl_matrix *R = gsl_matrix_alloc(n,n);
for(int i=0;i<n;i++){
	for(int j=0;j<n;j++){
		gsl_matrix_set(A,i,j,((double)rand()/RAND_MAX));
	}
}
printf("Opgave B\n Printer først A:\n");
printm(A);

printf("Laver så QR decomponition og printer Q: \n");
gsl_matrix* Q=A;
qr_gs_decomp(Q,R);
printm(Q);
printf("Printer R:\n");
printm(R);

gsl_matrix* QR = gsl_matrix_calloc(n,n);
gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, Q, R, 0.0, QR);

printf("Matrix QR=A printes:\n");
printm(QR);
printf("Og vi ser at den er lig A:\n");

gsl_matrix* B = gsl_matrix_alloc(n,n);
qr_gs_inverse(A, R, B);

printf("Printer B:\n");
printm(B);

printf("Udregner AB=I\n");
gsl_matrix* I = gsl_matrix_calloc(n,n);
gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, QR, B, 0.0, I); 
printf("Printer I:\n");
printm(I);
printf("og vi ser at I er identitetsmatriksen\n");

gsl_matrix_free(A);
gsl_matrix_free(R);
gsl_matrix_free(QR);
gsl_matrix_free(B);
gsl_matrix_free(I);

return 0;
}
