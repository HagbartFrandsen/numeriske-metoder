#include<stdio.h>
#include<math.h>

void f(int n, double x, double* y, double* dydx){
	dydx[0]=y[1];
	dydx[1]=2*(-1/x-1)*y[0];/* -(1/2)u''-(1/x)u = u */
	return;}

void rkstep23( void f(int n,double x,double* y,double* dydx),
int n, double x, double* yx, double h, double* yh, double* dy){
	int i; double k1[n],k2[n],k3[n],k4[n],yt[n];
	f(n,   x    ,yx,k1); 
	for(i=0;i<n;i++){
		yt[i]=yx[i]+1./2*k1[i]*h;
	}
	f(n,x+1./2*h,yt,k2); 
	for(i=0;i<n;i++){
		yt[i]=yx[i]+3./4*k2[i]*h;
	}
	f(n,x+3./4*h,yt,k3); 
	for(i=0;i<n;i++){
		yh[i]=yx[i]+(2./9 *k1[i]+1./3*k2[i]+4./9*k3[i])*h;
	}
	f(n,  x+h   ,yh,k4);  
	for(i=0;i<n;i++){
		yt[i]=yx[i]+(7./24*k1[i]+1./4*k2[i]+1./3*k3[i]+1./8*k4[i])*h;
		dy[i]=yh[i]-yt[i];
	}
}

void twostep(void f(int n, double x, double* y, double* dydx), int n, double xim, double xi, double* yim, double* yi, double h, double* dy, double* c, double* ybar, double* deltay){
	int i;
	f(n, xi, yi, dy);
	for(i=0;i<n;i++){
		c[i]=(yim[i]-yi[i]+dy[i]*(xi-xim))/((xi-xim)*(xi-xim));// (28)
	}
	for(i=0;i<n;i++){
		ybar[i]=yi[i]+dy[i]*h+c[i]*h*h; // (27)
	}
	for(i=0;i<n;i++){
		deltay[i]=c[i]*h*h; // (29)
	}
}

void twostepextra(void f(int n, double x, double* y, double* dydx), int n, double xim, double xi, double* yim, double* yi, double h, double* dy, double* ybarbar, double* deltay){
	int i;
	double c[n], ybar[n], ybart[n], d[n], fbar[n], t=xi+0.5*h;
	f(n, xi, yi, dy);
	for(i=0;i<n;i++){
		c[i]=(yim[i]-yi[i]+dy[i]*(xi-xim))/((xi-xim)*(xi-xim)); // (28)
	}
	for(i=0;i<n;i++){
		ybar[i]=yi[i]+dy[i]*h+c[i]*h*h; // (27) da x-x_i-1=h
	}
	for(i=0;i<n;i++){
		ybart[i]=yi[i]+dy[i]*(t-xi)+c[i]*(t-xi)*(t-xi); // (27) da x-x_i-1=h
	}
	f(n, t, ybart, fbar); // (31)
	for(i=0;i<n;i++){
		d[i]=(fbar[i]-dy[i]-2*c[i]*(t-xi))/(2*(t-xi)*(t-xim)+(t-xi)*(t-xi)); // (32)
	}
	for(i=0;i<n;i++){
		ybarbar[i]=ybar[i]+d[i]*h*h*(xi+h-xim); // (30)
	}
	for(i=0;i<n;i++){
		deltay[i]=ybarbar[i]-ybar[i]; // (33)
	}
}

int ode_driver_twostep(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max){
	int i,k=0; 
	double x,*y,s,err,normy,tol,a=xlist[0],yh[n],dy[n];
	x=xlist[k], y=ylist[k]; 
	if(x+h>b){
		h=b-x;
	}
	rkstep23(f,n,x,y,h,yh,dy);
	s=0;
	for(i=0;i<n;i++){
		s+=dy[i]*dy[i]; 
		err=sqrt(s);
	}
	s=0;
	for(i=0;i<n;i++){
		s+=yh[i]*yh[i];
		normy=sqrt(s);
	}
	tol=(normy*eps+acc)*sqrt(h/(b-a));
	if(err<tol){ /* accept step and continue */
		k++;
		if(k>max-1){
			return -k; /* uups */
		}
		xlist[k]=x+h;
		for(i=0;i<n;i++){
			ylist[k][i]=yh[i];
		}
	}
	if(err>0){
		h*=pow(tol/err,0.25)*0.95;
	}
	else{
		h*=2;
	}
	double xim, xi,* yim,* yi, ybar[n], c[n], deltay[n];
	while(xlist[k]<b){
		xim=xlist[k-1];
		xi=xlist[k];
		yim=ylist[k-1];
		yi=ylist[k];
		if(x+h>b){
			h=b-x;
		}
		twostep(f, n, xim, xi, yim, yi, h, dy, c, ybar, deltay);
		s=0;
		for(i=0;i<n;i++){
			s+=deltay[i]*deltay[i]; 
			err=sqrt(s);
		}
		s=0;
		for(i=0;i<n;i++){
			s+=ybar[i]*ybar[i];
			normy=sqrt(s);
		}
		tol=(normy*eps+acc)*sqrt(h/(b-a));
		if(err<tol){ // accept step and continue 
			k++;
			if(k>max-1){
				return -k; // uups 
			}
			xlist[k]=xi+h;
			for(i=0;i<n;i++){
				ylist[k][i]=ybar[i];
			}
		}
		if(err>0){
			h*=pow(tol/err,0.25)*0.95;
		}
		else{
			h*=2;
		}
	}
return k+1; /* return the number of entries in xlist/ylist */
}

int ode_driver_twostepextra(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max){
	int i,k=0; 
	double x,*y,s,err,normy,tol,a=xlist[0],yh[n],dy[n];
	x=xlist[k], y=ylist[k]; 
	if(x+h>b){
		h=b-x;
	}
	rkstep23(f,n,x,y,h,yh,dy);
	s=0;
	for(i=0;i<n;i++){
		s+=dy[i]*dy[i]; 
		err=sqrt(s);
	}
	s=0;
	for(i=0;i<n;i++){
		s+=yh[i]*yh[i];
		normy=sqrt(s);
	}
	tol=(normy*eps+acc)*sqrt(h/(b-a));
	if(err<tol){ /* accept step and continue */
		k++;
		if(k>max-1){
			return -k; /* uups */
		}
		xlist[k]=x+h;
		for(i=0;i<n;i++){
			ylist[k][i]=yh[i];
		}
	}
	if(err>0){
		h*=pow(tol/err,0.25)*0.95;
	}
	else{
		h*=2;
	}
	double xim, xi,* yim,* yi, ybarbar[n], deltay[n];
	while(xlist[k]<b){
		xim=xlist[k-1];
		xi=xlist[k];
		yim=ylist[k-1];
		yi=ylist[k];
		if(x+h>b){
			h=b-x;
		}
		twostepextra(f, n, xim, xi, yim, yi, h, dy, ybarbar, deltay);
		s=0;
		for(i=0;i<n;i++){
			s+=deltay[i]*deltay[i]; 
			err=sqrt(s);
		}
		s=0;
		for(i=0;i<n;i++){
			s+=ybarbar[i]*ybarbar[i];
			normy=sqrt(s);
		}
		tol=(normy*eps+acc)*sqrt(h/(b-a));
		if(err<tol){ // accept step and continue 
			k++;
			if(k>max-1){
				return -k; // uups 
			}
			xlist[k]=xi+h;
			for(i=0;i<n;i++){
				ylist[k][i]=ybarbar[i];
			}
		}
		if(err>0){
			h*=pow(tol/err,0.25)*0.95;
		}
		else{
			h*=2;
		}
	}
return k+1; /* return the number of entries in xlist/ylist */
}

int ode_driver_Runge_Kutta(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max){
	int i,k=0; 
	double x,*y,s,err,normy,tol,a=xlist[0],yh[n],dy[n];
	while(xlist[k]<b){
		x=xlist[k], y=ylist[k]; 
		if(x+h>b){
			h=b-x;
		}
		rkstep23(f,n,x,y,h,yh,dy);
		s=0;
		for(i=0;i<n;i++){
			s+=dy[i]*dy[i]; 
			err=sqrt(s);
		}
		s=0;
		for(i=0;i<n;i++){
			s+=yh[i]*yh[i];
			normy=sqrt(s);
		}
		tol=(normy*eps+acc)*sqrt(h/(b-a));
		if(err<tol){ /* accept step and continue */
			k++;
			if(k>max-1){
				return -k; /* uups */
			}
			xlist[k]=x+h;
			for(i=0;i<n;i++){
				ylist[k][i]=yh[i];
			}
		}
		if(err>0){
			h*=pow(tol/err,0.25)*0.95;
		}
		else{
			h*=2;
		}
	}
return k+1; /* return the number of entries in xlist/ylist */
} 
