void f(int n, double x, double* y, double* dydx);

void rkstep23( void f(int n, double x, double* y, double* dydx), int n, double x, double* yx, double h, double* yh, double* dy);

void twostep(void f(int n, double x, double* y, double* dydx), int n, double xim, double xi, double* yim, double* yi, double h, double* dy, double* c, double* ybar, double* deltay);

void twostepextra(void f(int n, double x, double* y, double* dydx), int n, double xim, double xi, double* yim, double* yi, double h, double* dy, double* ybarbar, double* deltay);

int ode_driver_twostep(void f(int n,double x,double*y,double*dydx), int n, double*xlist, double**ylist, double b, double h, double acc, double eps, int max);

int ode_driver_twostepextra(void f(int n,double x,double*y,double*dydx), int n, double* xlist, double **ylist, double b, double h, double acc, double eps, int max);

int ode_driver_Runge_Kutta(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max);
