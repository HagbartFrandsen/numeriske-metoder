#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"functions.h"

void fWOW(int n, double x, double* y, double* dydx){
	dydx[0]=y[1];
	dydx[1]=-x*y[0]-0.5*y[1];/* u''+1/2*u'+x*u=0 */
	return;}

int main(){

	printf("Opgave C:\n");
	printf("Tester løsninger til u''+1/2*u'+x*u=0 med Runge-Kutta, Two-Steps og Two-Steps with Extra Evaluation metoderne.\n");

	int n=2;
	int max=1000000;
	double*xlist_Runge_Kutta=(double*)calloc(max,sizeof(double));
	double**ylist_Runge_Kutta=(double**)calloc(max,sizeof(double*)); 
	for(int i=0;i<max;i++){
		ylist_Runge_Kutta[i]=(double*)calloc(n,sizeof(double)); 
	}
	double*xlist_twostep=(double*)calloc(max,sizeof(double));
	double**ylist_twostep=(double**)calloc(max,sizeof(double*)); 
	for(int i=0;i<max;i++){
		ylist_twostep[i]=(double*)calloc(n,sizeof(double)); 
	}
	double*xlist_twostepextra=(double*)calloc(max,sizeof(double));
	double**ylist_twostepextra=(double**)calloc(max,sizeof(double*)); 
	for(int i=0;i<max;i++){
		ylist_twostepextra[i]=(double*)calloc(n,sizeof(double)); 
	}
	double a=0, b=10, h=0.1, acc=1e-3, eps=acc;
	xlist_Runge_Kutta[0]=a; ylist_Runge_Kutta[0][0]=10; ylist_Runge_Kutta[0][1]=0;
	int k_Runge_Kutta = ode_driver_Runge_Kutta(fWOW,n,xlist_Runge_Kutta,ylist_Runge_Kutta,b,h,acc,eps,max);

	xlist_twostep[0]=a; ylist_twostep[0][0]=10; ylist_twostep[0][1]=0;
	int k_twostep = ode_driver_twostep(fWOW,n,xlist_twostep,ylist_twostep,b,h,acc,eps,max);

	xlist_twostepextra[0]=a; ylist_twostepextra[0][0]=10; ylist_twostepextra[0][1]=0;
	int k_twostepextra = ode_driver_twostepextra(fWOW,n,xlist_twostepextra,ylist_twostepextra,b,h,acc,eps,max);

	printf("Antal punkter der var nødvendige for vores tre metoder blev:\n");
	printf("Runge-Kutta \t Two-step \t Two-step with extra evaluation:\n");
	printf("%i     \t %i     \t %i\n",k_Runge_Kutta,k_twostep,k_twostepextra);
	printf("Hvilket betyder at middelværdien af step-størrelsen blev:\n");
	printf("Runge-Kutta \t Two-step \t Two-step with extra evaluation:\n");
	printf("%f \t %f \t %f\n",1/(float)k_Runge_Kutta,1/(float)k_twostep,1/(float)k_twostepextra);
	printf("Runge-Kutta er klart den bedste i dette tilfælde. Men ved Two-step metoden viser vi hvor stor en forbedring man kan få ved at tilføre det ekstra led, som vi gør i vores Two-step with extra evaluation.\n\n");

printf("Printer løsningen til u''+1/2*u'+x*u=0 med u(%g)=%g og u'(%g)=%g med acc=eps=%g ved brug af Runge-Kutta metoden.\n",a,ylist_Runge_Kutta[0][0],a,ylist_Runge_Kutta[0][1],acc);
	if(k_Runge_Kutta<0){
		printf("max steps reached in ode_driver\n");
	}
	printf("x\ty\n\n\n");
	for(int i=0;i<k_Runge_Kutta;i++){
		printf("%g       \t%g\n",xlist_Runge_Kutta[i],ylist_Runge_Kutta[i][0]);
	}

	printf("\n\n");
printf("Printer løsningen til u''+1/2*u'+x*u=0 med u(%g)=%g og u'(%g)=%g med acc=eps=%g ved brug af Two-Steps metoden, hvor Runge-Kutta metoden til at udføre det første step.\n",a,ylist_twostep[0][0],a,ylist_twostep[0][1],acc);
	if(k_twostep<0){
		printf("max steps reached in ode_driver\n");
	}
	printf("x\ty\n\n\n");
	for(int i=0;i<k_twostep;i++){
		printf("%g       \t%g\n",xlist_twostep[i],ylist_twostep[i][0]);
	}

	printf("\n\n");
printf("Printer løsningen til u''+1/2*u'+x*u=0 med u(%g)=%g og u'(%g)=%g med acc=eps=%g ved brug af Two-Steps with extra evaluation metoden, hvor Runge-Kutta metoden til at udføre det første step.\n",a,ylist_twostepextra[0][0],a,ylist_twostepextra[0][1],acc);
	if(k_twostepextra<0){
		printf("max steps reached in ode_driver\n");
	}
	printf("x\ty\n\n\n");
	for(int i=0;i<k_twostepextra;i++){
		printf("%g       \t%g\n",xlist_twostepextra[i],ylist_twostepextra[i][0]);
	}
return 0;
}

