Numeriske-metoder eksamen.
Af Hagbart Frandsen.

Jeg har valgt:
13 ODE: a two-step method

Implement a two-step method for solving ODE (as in the book). 

Opgave A:
Implementere two-step metoden og bruger den til at løse -(1/2)u''-(1/r)u=u.
Opgave B:
Implementere two-step with extra evaluation metoden og bruger den igen til at løse -(1/2)u''-(1/r)u=u.
Opgave C:
Sammenligner vores to tidligere metoder, med ren Runge-Kutta metoden til at løse u''+1/2*u'+x*u=0.

Har lavet plot til alle opgaverne for nemmere evaluering af resultaterne.
