#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include<math.h>
#include"functions.h"

int main(){
int points=10;

double xdata[10] ={0.100,0.145,0.211,0.307,0.447,0.649,0.944,1.372,1.995,2.900};
double ydata[10] ={12.644,9.235,7.377,6.460,5.555,5.896,5.673,6.964,8.896,11.355};
double dydata[10] ={0.858,0.359,0.505,0.403,0.683,0.605,0.856,0.351,1.083,1.002};

gsl_vector* x = gsl_vector_alloc(points);
gsl_vector* y = gsl_vector_alloc(points);
gsl_vector* dy = gsl_vector_alloc(points);
for(int i=0;i<points;i++){
	gsl_vector_set(x,i,xdata[i]);
	gsl_vector_set(y,i,ydata[i]);
	gsl_vector_set(dy,i,dydata[i]);
}

int n=x->size;
int m=3;
gsl_matrix* S=gsl_matrix_alloc(m,m);
gsl_vector* c= gsl_vector_alloc(n);
gsl_vector* yfit= gsl_vector_alloc(n);

lsFit(x,y,dy,c,S);

for(int i=0;i<points;i++){
	gsl_vector_set(yfit,i,(1/xdata[i])*gsl_vector_get(c,0)+gsl_vector_get(c,1)+xdata[i]*gsl_vector_get(c,2));
}

printf("Del A \n");
printf(" x \t y \t dy \n\n\n");
for(int i=0;i<points;i++){
printf("%g \t %g \t %g\n",gsl_vector_get(x,i),gsl_vector_get(y,i),gsl_vector_get(dy,i));
}
printf("\n\n");

printf(" x \t yfit \n\n\n");
for(int i=0;i<points;i++){
printf("%g \t %g \n",gsl_vector_get(x,i),gsl_vector_get(yfit,i));
}
printf("\n\n");

double x_min=0.09, x_max=3, dx=0.03;
// Usikkerheden på værdien er: sqrt(a^T*S*a). a er en vector med indgang (1/x,1,x). Så
gsl_vector* a=gsl_vector_alloc(m);
gsl_vector* Sa=gsl_vector_alloc(m);
printf("Del B \n");
printf("#x \t f  \t f+df \t f-df\n\n\n");
for(double k=x_min;k<x_max;k+=dx){
double f=0;
	for(int i=0; i<m; i++){
	gsl_vector_set(a,i,fitfunctions(i,k));
	f+=gsl_vector_get(c,i)*fitfunctions(i,k);
	}


// Matrix S gange vector a. Gemmes i vector Sa.
	matrix_vector_prod(m,m, Sa,S,a);
// Her laves prikprodukt mellem a og Sa:
double aTSa=0, df;
	for(int j=0; j<m; j++){
	aTSa+=gsl_vector_get(a,j)*gsl_vector_get(Sa,j);
	}
	df=pow(aTSa,0.5);
printf("%g \t %g   \t %g \t %g\n",k,f,f+df,f-df);
}

printf("\n\nUsikkerheden på parametrene er sqrt af diagonalindgangene. Så:\n");
// Usikkerheden på parametrene er sqrt af diagonalindgangene, så:
for(int i=0;i<m;i++){
printf("DeltaC%i=%g\n",i,pow(gsl_matrix_get(S,i,i),0.5));}


/* DEl C*/

n=sizeof(xdata)/sizeof(xdata[0]);  
//int 
m=3;
  gsl_vector * xx=gsl_vector_alloc(n);
  gsl_vector * yy=gsl_vector_alloc(n);
  gsl_vector * dyy=gsl_vector_alloc(n);
  gsl_vector * b=gsl_vector_alloc(n);
  gsl_matrix * A=gsl_matrix_alloc(n,3);
  
  for(int i=0;i<n;i++){gsl_vector_set(xx,i,xdata[i]),gsl_vector_set(yy,i,ydata[i]), gsl_vector_set(dyy,i,dydata[i]);}

printf("\n\n");
printf("Del C \n");
  for(int i=0; i<n;i++){
  double xi=gsl_vector_get(xx,i);
  double yi=gsl_vector_get(yy,i);
  
  gsl_vector_set(b,i,yi);
  for(int k=0; k<m;k++){gsl_matrix_set(A,i,k,fitfunctions(k,xi));} 
  }  
  
/* singular-value decomposition. */ 
S = gsl_matrix_calloc(m,m);
  gsl_matrix * V = gsl_matrix_calloc(m,m);
  gsl_matrix * U = gsl_matrix_alloc(n,m);
c = gsl_vector_alloc(m);
  gsl_matrix * VS_min2_VT = gsl_matrix_alloc(m,m);
  sing_val_de(A,U,S,V);

printf("#x \t f  \t f+df \t f-df\n\n\n");
  sing_val_de_solve(U,S,V,b,c, VS_min2_VT);
  for(double z=0.1;z<=3;z+=0.01){
    double s=0, sp=0, sm=0;
    for(int k=0; k<m;k++){
    s+=gsl_vector_get(c,k)*fitfunctions(k,z);
    sp+=(gsl_vector_get(c,k)+gsl_matrix_get(VS_min2_VT,k,k))*fitfunctions(k,z);
    sm+=(gsl_vector_get(c,k)-gsl_matrix_get(VS_min2_VT,k,k))*fitfunctions(k,z);
    }
    printf("%g \t %g   \t %g   \t %g \n",z,s,sp,sm);
  }

/* Ordinary least squar */
printf("\n\n");
printf("#x \t f  \t f+df \t f-df\n\n\n");
  gsl_vector * c2=gsl_vector_alloc(m);
  gsl_matrix * VS_min2_VT2=gsl_matrix_calloc(m,m);
  ord_lst_sqr_fit(A,b,xx,yy,dyy,c2,VS_min2_VT2);
  for(double z=0.1;z<=3;z+=0.1){
    double s=0, sp=0, sm=0;
    for(int k=0; k<m;k++){
    s+=gsl_vector_get(c2,k)*fitfunctions(k,z);
    sp+=(gsl_vector_get(c2,k)+gsl_matrix_get(VS_min2_VT2,k,k))*fitfunctions(k,z);
    sm+=(gsl_vector_get(c2,k)-gsl_matrix_get(VS_min2_VT2,k,k))*fitfunctions(k,z);
    }
    printf("%g \t %g \t %g   \t %g \n",z,s,sp,sm);
  }

gsl_matrix_free(S);
gsl_vector_free(x);
gsl_vector_free(y);
gsl_vector_free(dy);
gsl_vector_free(c);
gsl_vector_free(yfit);
gsl_vector_free(a);
gsl_vector_free(Sa);
gsl_vector_free(xx);
gsl_vector_free(yy);
gsl_vector_free(dyy);
gsl_vector_free(b);
gsl_matrix_free(A);

return 0;
}
