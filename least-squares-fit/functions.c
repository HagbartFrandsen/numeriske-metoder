#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include<math.h>
#include"functions.h"

double dot_prod(gsl_matrix* A,int i,int j){
double dotprod=0;
int length=A->size1;
for(int k=0;k<length;k++){
dotprod+=gsl_matrix_get(A,k,i)*gsl_matrix_get(A,k,j);
}
return dotprod;
}

void matrix_coloum_get(gsl_matrix* A, gsl_vector* b, int i){
for(int j=0;j<A->size1;j++){
gsl_vector_set(b,j,gsl_matrix_get(A,j,i));
}
}

void vector_rescale(gsl_vector* a, gsl_vector* b, double d){
for(int j=0;j<a->size;j++){
gsl_vector_set(a,j,gsl_vector_get(a,j)-gsl_vector_get(b,j)*d);
}
}

void element_funktion(int n, int m, gsl_matrix* OUT, gsl_matrix* L, gsl_matrix* R){
double sum=0;
for(int i=0;i<L->size2;i++){
sum+=gsl_matrix_get(L,n,i)*gsl_matrix_get(R,i,m); //sum+=L(n,i)*R(i,m)
}
gsl_matrix_set(OUT,n,m,sum);
}

void matrix_multiplication(gsl_matrix* OUT, gsl_matrix* L, gsl_matrix* R){

int m=OUT->size2, n=OUT->size1;

for(int j=0;j<m;j++){
for(int i=0;i<n;i++){
element_funktion(i,j,OUT,L,R);
}
}
}

void matrix_coloum_set(gsl_matrix* A, gsl_vector* b, int i){
for(int j=0;j<A->size1;j++){
gsl_matrix_set(A,j,i,gsl_vector_get(b,j));
}
}

void matrix_vector_prod(int n, int m, gsl_vector* QTb, gsl_matrix* QT, gsl_vector* b){ //n er antal rækker i QT, m er antallet af søjler i QT.
for(int j=0;j<n;j++){
double sum=0;
for(int i=0;i<m;i++){
sum+=gsl_matrix_get(QT,j,i)*gsl_vector_get(b,i); 
}
gsl_vector_set(QTb,j,sum);
}
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
int m=A->size2;
gsl_vector *ai=gsl_vector_alloc(A->size1);
gsl_vector *aj=gsl_vector_alloc(A->size1);

for(int i=0;i<m;i++){
gsl_matrix_set(R,i,i,pow(dot_prod(A,i,i),0.5)); // Her findes R_ii via prikprodukt.
matrix_coloum_get(A, ai,i); // Her indsættes værdier fra den i'te søje ind i vektoren ai.
gsl_vector_scale(ai,1.0/gsl_matrix_get(R,i,i)); // Skaleres med 1/R_ii
matrix_coloum_set(A,ai,i); // Nye a_i-værdi indsættes.

for(int j=i+1;j<m;j++){
matrix_coloum_get(A,aj,j); // Her indsættes værdier fra den j'te søjle ind i vektoren aj.
gsl_matrix_set(R,i,j,dot_prod(A,i,j)); // Her udregnes R_ij

// I linjen nedenfor udføres følgende operation: aj=aj-ai*R_ij
vector_rescale(aj, ai, gsl_matrix_get(R,i,j));


matrix_coloum_set(A,aj,j); // Og det sættes ind i matrix A på søjle j.
}
}
gsl_vector_free(ai);
gsl_vector_free(aj);
}

void qr_gs_solve(gsl_matrix* Q,gsl_matrix* R, gsl_vector* b){
// Q^T findes:
int n=Q->size1, m=Q->size2;
gsl_matrix* QT = gsl_matrix_alloc(m,n);
gsl_matrix_transpose_memcpy(QT,Q);

// Nu udregnes Q^T b:
gsl_vector* QTb = gsl_vector_alloc(m);
matrix_vector_prod(m,n,QTb,QT,b);

b->size=m;
// Back-substitution:
for(int i=QTb->size-1; i>=0; i--){
double s=gsl_vector_get(QTb,i);
for(int k=i+1;k<QTb->size;k++){ s-=gsl_matrix_get(R,i,k)*gsl_vector_get(b,k);}
gsl_vector_set(b,i,s/gsl_matrix_get(R,i,i));
}
gsl_matrix_free(QT);
gsl_vector_free(QTb);
}

void qr_gs_inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B){
int n=Q->size1;
int m=Q->size2;
gsl_vector* e_i=gsl_vector_alloc(n);


for(int i=0;i<m;i++){
// Værdien for e_i indsættes:
	for(int k=0;k<n;k++){gsl_vector_set(e_i,k,0);} // Alle elementer sættes til 0.
	gsl_vector_set(e_i,i,1); // element i sættes til 1.

qr_gs_solve(Q,R,e_i); // Udregnes og resultatet indsættes i vektor e_i.

	
matrix_coloum_set(B,e_i,i); // e_i indsættes i B på række i.
}
gsl_vector_free(e_i);
}

/* Least Square Fit */
double fitfunctions(int i, double x){
	switch(i){
	case 0: return 1.0/x; break;
	case 1: return 1.0; break;
	case 2: return x; break;	
	default: {fprintf(stderr,"fitfunctions: wrong i:%i",i); return NAN;}	
	}
}

void lsFit(gsl_vector* x, gsl_vector* y, gsl_vector* dy, gsl_vector* b, gsl_matrix* R){
int n=x->size;
int m=3;
gsl_matrix* A=gsl_matrix_alloc(n,m);

for(int i=0;i<n;i++){
	gsl_vector_set(b,i,gsl_vector_get(y,i)/gsl_vector_get(dy,i));
	for(int k=0;k<m;k++){
	double tja=fitfunctions(k,gsl_vector_get(x,i));
	gsl_matrix_set(A,i,k,tja/gsl_vector_get(dy,i));
	}
}

qr_gs_decomp(A,R);
	
qr_gs_solve(A,R,b);

gsl_matrix* I=gsl_matrix_alloc(m,m);
gsl_matrix_set_identity(I);
gsl_matrix* RInv=gsl_matrix_alloc(m,m);
gsl_matrix* RInvTransp=gsl_matrix_alloc(m,m);

qr_gs_inverse(I,R,RInv);

gsl_matrix_transpose_memcpy(RInvTransp,RInv);

matrix_multiplication(R,RInv,RInvTransp); // S gemmes i matrix R. S er covariant matrix.
	
gsl_matrix_free(A);
gsl_matrix_free(I);
gsl_matrix_free(RInv);
gsl_matrix_free(RInvTransp);
}

int jacobi_cyc(gsl_matrix * A, gsl_vector * e, gsl_matrix * V){
  int n=(*A).size1, changed, rotations=0;
  gsl_matrix_set_identity(V);

  for(int i=0;i<n;i++){gsl_vector_set(e,i,gsl_matrix_get(A,i,i));}
   
  do{changed=0; int p, q;
   for(p=0; p<n; p++)for(q=p+1;q<n;q++){
   double A_pp=gsl_vector_get(e,p);
   double A_qq=gsl_vector_get(e,q);
   double A_pq=gsl_matrix_get(A,p,q);
   double phi=1.0/2.0*atan2(2*A_pq,A_qq-A_pp);
   double s=sin(phi), c=cos(phi);
   double A_pp1=pow(c,2)*A_pp-2*s*c*A_pq+pow(s,2)*A_qq;
   double A_qq1=pow(s,2)*A_pp+2*s*c*A_pq+pow(c,2)*A_qq;
     if(A_qq1!=A_qq || A_pp1!=A_pp){changed=1; rotations++; 
      gsl_vector_set(e,p,A_pp1); 
      gsl_vector_set(e,q,A_qq1); 
      gsl_matrix_set(A,p,q,0.0);
      for(int i=0;i<p;i++){
       double A_ip=gsl_matrix_get(A,i,p), A_iq=gsl_matrix_get(A,i,q);
       gsl_matrix_set(A,i,p,c*A_ip-s*A_iq);
       gsl_matrix_set(A,i,q,s*A_ip+c*A_iq);
      }
      for(int i=p+1;i<q;i++){
       double A_pi=gsl_matrix_get(A,p,i), A_iq=gsl_matrix_get(A,i,q);
       gsl_matrix_set(A,p,i,c*A_pi-s*A_iq);
       gsl_matrix_set(A,i,q,s*A_pi+c*A_iq);
      }
      for(int i=q+1;i<n;i++){
       double A_pi=gsl_matrix_get(A,p,i), A_qi=gsl_matrix_get(A,q,i);
       gsl_matrix_set(A,p,i,c*A_pi-s*A_qi);
       gsl_matrix_set(A,q,i,s*A_pi+c*A_qi);
      } 
      for(int i=0;i<n;i++){
       double A_ip=gsl_matrix_get(V,i,p), A_iq=gsl_matrix_get(V,i,q);
       gsl_matrix_set(V,i,p,c*A_ip-s*A_iq);
       gsl_matrix_set(V,i,q,s*A_ip+c*A_iq);
      }
     }
   }
  }while(changed!=0);
  return rotations;
}

void sing_val_de(gsl_matrix *A, gsl_matrix * U, gsl_matrix * S, gsl_matrix * V ){
  int n=(*A).size1, j=(*S).size1;
  gsl_matrix * ATA=gsl_matrix_alloc(j,j);
  gsl_matrix * D = gsl_matrix_calloc(j,j);
  gsl_vector * e = gsl_vector_alloc(j);
  
  gsl_blas_dgemm (CblasTrans,CblasNoTrans, 1.0,A,A,0.0,ATA);// Generating ATA
  jacobi_cyc(ATA,e,V);

  for(int i=0; i<j; i++)gsl_matrix_set(D,i,i,gsl_vector_get(e,i));
  
  for(int i=0; i<j;i++){gsl_matrix_set(S,i,i,sqrt(gsl_matrix_get(D,i,i)));}
  
  gsl_matrix * AV=gsl_matrix_calloc(n,j);
  gsl_matrix * D_sqrt_inv=gsl_matrix_calloc(j,j);
  gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,A,V,0.0,AV);// Generating ATA
  for(int i=0; i<j;i++){gsl_matrix_set(D_sqrt_inv,i,i,1.0/sqrt(gsl_matrix_get(D,i,i)));}
  gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,AV,D_sqrt_inv,0.0,U);

  gsl_matrix_free(ATA);
  gsl_vector_free(e);
  gsl_matrix_free(D);
  gsl_matrix_free(AV);
}

void sing_val_de_solve(gsl_matrix * U, gsl_matrix * S, gsl_matrix * V, gsl_vector * b, gsl_vector * c, gsl_matrix * VS_min2_VT  ){
  int m=(*U).size2;
  gsl_vector * UTb=gsl_vector_alloc(m);
  gsl_matrix * Sinv=gsl_matrix_calloc(m,m);
  gsl_vector * y=gsl_vector_alloc(m);
  gsl_blas_dgemv (CblasTrans,1.0,U,b,0.0,UTb);
  
  for(int i=0; i<m;i++)gsl_matrix_set(Sinv,i,i,1.0/gsl_matrix_get(S,i,i));
  
  gsl_blas_dgemv (CblasNoTrans, 1.0,Sinv,UTb,0.0,y);
  
  gsl_blas_dgemv (CblasNoTrans, 1.0,V,y,0.0,c);
  
   /* calculating covariance matrix */
   gsl_matrix * S_min2=gsl_matrix_calloc(m,m);
   gsl_matrix * VS_min2 = gsl_matrix_alloc(m,m);
   for(int i=0; i<m;i++)gsl_matrix_set(S_min2,i,i,1.0/pow(gsl_matrix_get(S,i,i),2));
   gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,V,S_min2,0.0,VS_min2);
   gsl_blas_dgemm (CblasNoTrans,CblasTrans, 1.0,VS_min2,V,0.0,VS_min2_VT);
  
  gsl_vector_free(UTb);
  gsl_matrix_free(Sinv);
  gsl_vector_free(y);
  gsl_matrix_free(S_min2);  
  gsl_matrix_free(VS_min2);
 
}

void ord_lst_sqr_fit(gsl_matrix * A, gsl_vector * b, gsl_vector *xx, gsl_vector * yy, gsl_vector *dyy   , gsl_vector * c,gsl_matrix * VS_min2_VT){
  int n=(*A).size1, m=(*A).size2;
  for(int i=0; i<n;i++){
  double xi=gsl_vector_get(xx,i);
  double yi=gsl_vector_get(yy,i);
  double dyi=gsl_vector_get(dyy,i);
  //double dyi=gsl_vector_get(dy,i);
  gsl_vector_set(b,i,yi/dyi);
   for(int k=0; k<m;k++){gsl_matrix_set(A,i,k,fitfunctions(k,xi)/dyi);} 
  }
 
  gsl_matrix * S = gsl_matrix_calloc(m,m);
  gsl_matrix * V = gsl_matrix_calloc(m,m);
  gsl_matrix * U = gsl_matrix_alloc(n,m);
  sing_val_de(A,U,S,V);
  sing_val_de_solve(U,S,V,b,c,VS_min2_VT );

 gsl_matrix_free(S);
 gsl_matrix_free(V);
 gsl_matrix_free(U);
}
