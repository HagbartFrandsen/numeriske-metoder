#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

void printv(gsl_vector *b);

void newton(void f(gsl_vector* x,gsl_vector* fx), gsl_vector* x, double dx, double eps);
void newton_with_jacobian(void f(gsl_vector* x,gsl_vector* fx), void jacobian(gsl_vector* x, gsl_matrix* J), gsl_vector* x, double dx, double eps);

int newton_with_Jacobian_refined(void f(gsl_vector* x, gsl_vector* fx),void Jmatrix(gsl_vector* x, gsl_matrix* J), gsl_vector* xstart, double dx, double epsilon);
