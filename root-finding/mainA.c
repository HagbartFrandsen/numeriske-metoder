#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include"functions.h"

int newton (void f(gsl_vector* x,gsl_vector* fx), gsl_vector* x, double dx, double eps);

int main() {
	int ncalls=0;
	void f(gsl_vector* p,gsl_vector* fx){
		ncalls++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x); /* d/dx af rosenbrock */
		gsl_vector_set(fx,1, 100*2*(y-x*x)); /* d/dy af rosenbrock */
		}
	gsl_vector* x=gsl_vector_alloc(2);
	gsl_vector_set(x,0,2);
	gsl_vector_set(x,1,-1);
	gsl_vector* fx=gsl_vector_alloc(2);
	printf("Opgave A:\n");
	printf("Extremum af Rosenbrock's funktion:\n");
	printf("Start vektor x: \n");
	printv(x);
	f(x,fx);
	printf("Start f(x): \n");
	printv(fx);
	newton(f,x,1e-5,1e-5);
	printf("ncalls = %i\n",ncalls);
	gsl_vector_fprintf(stderr,x,"%g");
	printf("løsning x: \n");
	printv(x);
	f(x,fx);
	printf("løsning f(x): \n");
	printv(fx);

	ncalls=0;
	void f2(gsl_vector* p,gsl_vector* fx){
		ncalls++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 4*x*(pow(x,2)+y-11)+2*(x+pow(y,2)-7)); /* d/dx af himmelblau */
		gsl_vector_set(fx,1, 2*pow(pow(x,2)+y-11,2)+4*y*pow(x+pow(y,2)-7,2)); /* d/dy af himmelblau */
		}
	x=gsl_vector_alloc(2);
	gsl_vector_set(x,0,5);
	gsl_vector_set(x,1,3);
	fx=gsl_vector_alloc(2);
	printf("\nExtremum af Himmelblau's funktion:\n");
	printf("Start vektor x: \n");
	printv(x);
	f2(x,fx);
	printf("Start f(x): \n");
	printv(fx);
	newton(f2,x,1e-5,1e-5);
	printf("ncalls = %i\n",ncalls);
	gsl_vector_fprintf(stderr,x,"%g");
	printf("løsning x: \n");
	printv(x);
	f2(x,fx);
	printf("løsning f(x): \n");
	printv(fx);


	ncalls=0;
	double pi=3.1415926535897932384626433832795028841971693993751058209749445923078164062862;
	void f3(gsl_vector* p,gsl_vector* fx){
		ncalls++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, (2*cos(x)*cos(y)*(x-pi)+sin(x)*cos(y))*exp(-pow(y-pi,2)-pow(x-pi,2))	); /* d/dx af easom */
		gsl_vector_set(fx,1, (2*cos(x)*cos(y)*(y-pi)+cos(x)*sin(y))*exp(-pow(y-pi,2)-pow(x-pi,2))	); /* d/dy af easom */
		}
	x=gsl_vector_alloc(2);
	gsl_vector_set(x,0,3);
	gsl_vector_set(x,1,3);
	fx=gsl_vector_alloc(2);
	printf("\nExtremum af Easom funktion:\n");
	printf("Start vektor x: \n");
	printv(x);
	f3(x,fx);
	printf("Start f(x): \n");
	printv(fx);
	newton(f3,x,1e-5,1e-5);
	printf("ncalls = %i\n",ncalls);
	gsl_vector_fprintf(stderr,x,"%g");
	printf("løsning x: \n");
	printv(x);
	f3(x,fx);
	printf("løsning f(x): \n");
	printv(fx);

gsl_vector_free(x);
gsl_vector_free(fx);

return 0;
}
