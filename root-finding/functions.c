#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>
#include<math.h>
#include<assert.h>

double dot_product(gsl_matrix* A,int i,int j){
double dot=0;
int length=A->size1;
for(int k=0;k<length;k++){
	dot+=gsl_matrix_get(A,k,i)*gsl_matrix_get(A,k,j);
}
return dot;
}

void matrix_column_get(gsl_matrix* A, gsl_vector* b, int i){
for(int j=0;j<A->size1;j++){
	gsl_vector_set(b,j,gsl_matrix_get(A,j,i));
}
}

void matrix_column_set(gsl_matrix* A, gsl_vector* b, int i){
for(int j=0;j<A->size1;j++){
	gsl_matrix_set(A,j,i,gsl_vector_get(b,j));
}
}

void printv(gsl_vector *b){
	for(int r=0;r<b->size;r++){
		printf("%.3g\t",gsl_vector_get(b,r));
		printf("\n");
	}
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
int m=A->size2; /* antal kolonner m */
gsl_vector *ai=gsl_vector_alloc(A->size1); /* frigør plads til 2 vektorer, med samme antal indgange som antal rækker i A rækker */
gsl_vector *aj=gsl_vector_alloc(A->size1);

for(int i=0;i<m;i++){
	gsl_matrix_set(R,i,i,pow(dot_product(A,i,i),0.5)); /* prikproduktet laver værdierne for Rii */
	matrix_column_get(A, ai,i); /* Her findes værdier til den i'te søje fra vektoren ai. */
	gsl_vector_scale(ai,1.0/gsl_matrix_get(R,i,i)); /* ganger med 1/R_ii */
	matrix_column_set(A,ai,i); /* de nye værdier rettes nu for A */

	for(int j=i+1;j<m;j++){
		gsl_matrix_set(R,i,j,dot_product(A,i,j)); /* prikproduktet laver værdierne for Rij */
		matrix_column_get(A,aj,j); /* Her findes værdier til den j'te søje fra vektoren aj. */

		for(int k=0;k<aj->size;k++){
			gsl_vector_set(aj,k,gsl_vector_get(aj,k)-gsl_vector_get(ai,k)*gsl_matrix_get(R,i,j)); /* aj=aj-ai*R_ij */
		}
		matrix_column_set(A,aj,j); // Og det sættes ind i matrix A på søjle j.
	}
}
gsl_vector_free(ai);
gsl_vector_free(aj);
}


void qr_gs_solve(gsl_matrix* Q,gsl_matrix* R, gsl_vector* b){
int n=Q->size1, m=Q->size2; /* finder Q^T */
gsl_matrix* QT = gsl_matrix_alloc(m,n);
gsl_matrix_transpose_memcpy(QT,Q);

gsl_vector* QTb = gsl_vector_alloc(m);
gsl_blas_dgemv (CblasNoTrans, 1.0, QT, b, 0.0, QTb); /* udregner Q^Tb */

b->size=m;
// Back-substitution:
for(int i=QTb->size-1; i>=0; i--){
	double s=gsl_vector_get(QTb,i);
	for(int k=i+1;k<QTb->size;k++){ s-=gsl_matrix_get(R,i,k)*gsl_vector_get(b,k);}
		gsl_vector_set(b,i,s/gsl_matrix_get(R,i,i));
	}
gsl_matrix_free(QT);
gsl_vector_free(QTb);
}

void givens_qr(gsl_matrix* A){
for(int q=0; q<A->size2; q++){
	for(int p=q+1; p<A->size1; p++){
		double theta=atan2(gsl_matrix_get(A, p, q), gsl_matrix_get(A, q, q));
		for(int k=q; k<A->size2; k++){
			double xq=gsl_matrix_get(A, q, k), xp=gsl_matrix_get(A, p, k);
			gsl_matrix_set(A, q, k, xq*cos(theta)+xp*sin(theta));
			gsl_matrix_set(A, p, k,-xq*sin(theta)+xp*cos(theta));
		}
		gsl_matrix_set(A, p, q, theta);
	}
}
}

void givens_qr_QTvec(gsl_matrix* QR, gsl_vector* v){
assert(QR->size1 == v->size);
for(int q=0; q<QR->size2; q++){
	for(int p=q+1; p<QR->size1; p++){
		double theta=gsl_matrix_get(QR, p, q);
		double vq=gsl_vector_get(v, q), vp=gsl_vector_get(v , p);
		gsl_vector_set(v, q, vq*cos(theta)+vp*sin(theta));
		gsl_vector_set(v, p, -vq*sin(theta)+vp*cos(theta));
	}
}
}

void givens_qr_solve(gsl_matrix* QR, gsl_vector* b, gsl_vector* x){
givens_qr_QTvec(QR,b); 
	for (int i=QR->size2-1; i>=0; i--){ //back-substitution
	double s=0;
		for(int k=i+1; k<QR->size2; k++){ s+=gsl_matrix_get(QR,i,k)*gsl_vector_get(x,k);}
		gsl_vector_set(x,i,(gsl_vector_get(b,i)-s)/gsl_matrix_get(QR,i,i));
	}
}

void gramschmidt_qr_solve(gsl_matrix *Q, gsl_matrix *R, gsl_vector *b, gsl_vector *x){
	int m=R->size1;
	gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x); // Q^T*b -> x
	for(int i=m-1;i>=0;i--){                  // backsustitution -> x
		double s=0;
		for(int k=i+1;k<m;k++) s+=gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
		gsl_vector_set(x,i,(gsl_vector_get(x,i)-s)/gsl_matrix_get(R,i,i));
		}
	}

void newton(void f(gsl_vector* x,gsl_vector* fx), gsl_vector* x, double dx, double eps){
	int n=x->size;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);
	while(1){
		f(x,fx);
		for (int j=0;j<n;j++){
			gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);
			f(x,df);
			gsl_vector_sub(df,fx); /* df=f(x+dx)-f(x) */
			for(int i=0;i<n;i++) gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx);
			gsl_vector_set(x,j,gsl_vector_get(x,j)-dx);
			}
		qr_gs_decomp(J,R);
		gramschmidt_qr_solve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1);
		double s=1;
		while(1){
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx);
			f(z,fz);
			if( gsl_blas_dnrm2(fz)<(1-s/2)*gsl_blas_dnrm2(fx) || s<0.02 ) break;
			s*=0.5;
			gsl_vector_scale(Dx,0.5);
			}
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(fx,fz);
		if( gsl_blas_dnrm2(Dx)<dx || gsl_blas_dnrm2(fx)<eps ) break;
		}
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(fz);
	gsl_vector_free(z);
	gsl_vector_free(df);
	gsl_vector_free(Dx);
}

void newton_with_jacobian(void f(gsl_vector* x,gsl_vector* fx), void jacobian(gsl_vector* x, gsl_matrix* J), gsl_vector* x, double dx, double eps){
	int n=x->size;
gsl_matrix* J=gsl_matrix_alloc(n,n);
gsl_vector* Dx=gsl_vector_alloc(n);
gsl_vector* y=gsl_vector_alloc(n);
gsl_vector* fy=gsl_vector_alloc(n);
gsl_vector* fx=gsl_vector_alloc(n);
gsl_vector* df=gsl_vector_alloc(n);
int steps=0;
do{
steps++;
f(x,fx);
jacobian(x,J);
givens_qr(J);
gsl_vector_scale(fx,-1.0);
givens_qr_solve(J,fx,Dx);
gsl_vector_scale(fx,-1.0);
double lambda=2;

	do{
	lambda/=2;
	gsl_vector_memcpy(y,Dx);
	gsl_vector_scale(y,lambda);
	gsl_vector_add(y,x);
	f(y,fy);
	}while(gsl_blas_dnrm2(fy)>(1-lambda/2)*gsl_blas_dnrm2(fx) && lambda>0.02);
gsl_vector_memcpy(x,y);		
gsl_vector_memcpy(fx,fy);
}while(gsl_blas_dnrm2(Dx)>dx && gsl_blas_dnrm2(fx)>eps);

gsl_matrix_free(J);
gsl_vector_free(Dx);
gsl_vector_free(y);
gsl_vector_free(fy);
gsl_vector_free(fx);
gsl_vector_free(df);
return steps;
}

int newton_with_Jacobian_refined(void f(gsl_vector* x, gsl_vector* fx),void Jmatrix(gsl_vector* x, gsl_matrix* J), gsl_vector* xstart, double dx, double epsilon){
int n=xstart->size;
gsl_matrix* J=gsl_matrix_alloc(n,n);
gsl_vector* Dx=gsl_vector_alloc(n);
gsl_vector* y=gsl_vector_alloc(n);
gsl_vector* fy=gsl_vector_alloc(n);
gsl_vector* fx=gsl_vector_alloc(n);
gsl_vector* df=gsl_vector_alloc(n);
int steps=0;
do{
steps++;
f(xstart,fx);
Jmatrix(xstart,J);
givens_qr(J);
gsl_vector_scale(fx,-1.0);
givens_qr_solve(J,fx,Dx);
gsl_vector_scale(fx,-1.0);
double lambda=0.99;
	do{
	gsl_vector_memcpy(y,Dx);
	gsl_vector_scale(y,lambda);
	gsl_vector_add(y,xstart);
	f(y,fy);

	double g0=0.5*pow(gsl_blas_dnrm2(fx),2);
	double gm0=-pow(gsl_blas_dnrm2(fx),2);
	double glambda=0.5*pow(gsl_blas_dnrm2(fy),2);

	double c=(glambda-g0-gm0*lambda)/pow(lambda,2);
	lambda=-gm0/(2.0*c);

	}while(gsl_blas_dnrm2(fy)>(1-lambda/2)*gsl_blas_dnrm2(fx) && lambda>0.02);
gsl_vector_memcpy(xstart,y);		
gsl_vector_memcpy(fx,fy);
}while(gsl_blas_dnrm2(Dx)>dx && gsl_blas_dnrm2(fx)>epsilon);

gsl_matrix_free(J);
gsl_vector_free(Dx);
gsl_vector_free(y);
gsl_vector_free(fy);
gsl_vector_free(fx);
gsl_vector_free(df);
return steps;
}
