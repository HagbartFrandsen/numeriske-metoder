#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include"functions.h"

void J_Rosenbrock(gsl_vector* x, gsl_matrix* J){
double x0=gsl_vector_get(x,0), x1=gsl_vector_get(x,1);
double J00,J01,J10,J11;
J00=400*3*pow(x0,2)-400*(x1-0.005);
J01=-400*x0;
J10=-200*2*x0;
J11=200;
gsl_matrix_set(J,0,0,J00);
gsl_matrix_set(J,0,1,J01);
gsl_matrix_set(J,1,0,J10);
gsl_matrix_set(J,1,1,J11);
}


void J_Himmelblau(gsl_vector* x, gsl_matrix* J){
double x0=gsl_vector_get(x,0), x1=gsl_vector_get(x,1);
double J00,J01,J10,J11;
J00=12*pow(x0,2)+4*(x1-11);
J01=4*x1+4*x0;
J10=4*x1+4*x0;
J11=12*pow(x1,2)+4*(x0-7);
gsl_matrix_set(J,0,0,J00);
gsl_matrix_set(J,0,1,J01);
gsl_matrix_set(J,1,0,J10);
gsl_matrix_set(J,1,1,J11);
}


void f_Himmelblau(gsl_vector* z, gsl_vector* fx){
double x=gsl_vector_get(z,0), y=gsl_vector_get(z,1);
double fx0=4*x*(pow(x,2)+y-11)+2*(x+pow(y,2)-7);
double fx1=2*pow(pow(x,2)+y-11,2)+4*y*pow(x+pow(y,2)-7,2);
gsl_vector_set(fx,0,fx0);
gsl_vector_set(fx,1,fx1);
}

int main(){
	printf("\nOpgave B:\n");

	int ncalls=0;

	gsl_vector* fx=gsl_vector_alloc(2);
	gsl_vector* x=gsl_vector_alloc(2);
	gsl_vector_set(x,0,2);
	gsl_vector_set(x,1,-1);

void f1(gsl_vector* p,gsl_vector* fx){
		ncalls++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x); /* d/dx af rosenbrock */
		gsl_vector_set(fx,1, 100*2*(y-x*x)); /* d/dy af rosenbrock */
		}

	printf("Extremum af Rosenbrock's funktion:\n");
	printf("Start vektor x: \n");
	printv(x);
	f1(x,fx);
	printf("Start f(x): \n");
	printv(fx);
newton_with_jacobian(f1,J_Rosenbrock,x,1e-5,1e-5);
	printf("ncalls = %i\n",ncalls);
	gsl_vector_fprintf(stderr,x,"%g");
	printf("løsning x: \n");
	printv(x);
	f1(x,fx);
	printf("løsning f(x): \n");
	printv(fx);


	ncalls=0;

	gsl_vector_set(x,0,5);
	gsl_vector_set(x,1,3);

void f2(gsl_vector* z, gsl_vector* fx){ /* Himmelblau's function */
ncalls++;
double x=gsl_vector_get(z,0), y=gsl_vector_get(z,1);
double fx0=4*pow(x,3)+4*x*(y-11)+2*(pow(y,2)-7);
double fx1=4*pow(y,3)+4*y*(x-7)+2*pow(x,2)-22;
gsl_vector_set(fx,0,fx0);
gsl_vector_set(fx,1,fx1);
}

	printf("\nExtremum af Himmelblau's funktion:\n");
	printf("Start vektor x: \n");
	printv(x);
	f2(x,fx);
	printf("Start f(x): \n");
	printv(fx);
newton_with_jacobian(f2,J_Himmelblau,x,1e-5,1e-5);
	printf("ncalls = %i\n",ncalls);
	gsl_vector_fprintf(stderr,x,"%g");
	printf("løsning x: \n");
	printv(x);
	f2(x,fx);
	printf("løsning f(x): \n");
	printv(fx);

gsl_vector_free(x);
gsl_vector_free(fx);

return 0;
}
