#include<stdio.h>
#include<math.h>

void f(int n, double x, double* y, double* dydx){
	dydx[0]=y[1];
	dydx[1]=2*(-1/x-1)*y[0];/* -(1/2)u''-(1/r)u = u */
	return;}

void rkstep12(void f(int n, double x, double*yx, double*dydx),
int n, double x, double* yx, double h, double* yh, double* dy){
  double k0[n],yt[n],k12[n]; /* VLA: gcc -std=c99 */
  f(n,x    ,yx,k0);  for(int i=0;i<n;i++) yt[i]=yx[i]+ k0[i]*h/2;
  f(n,x+h/2,yt,k12); for(int i=0;i<n;i++) yh[i]=yx[i]+k12[i]*h;
  for(int i=0;i<n;i++) dy[i]=(k0[i]-k12[i])*h/2;
}


void ode_driverA(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max){
int i,k=0; 
double x,*y,s,err,normy,tol,a=xlist[0],yh[n],dy[n];
while(xlist[0]<b){
	x=xlist[0], y=ylist[0]; 
	if(x+h>b){
		h=b-x;
	}
	rkstep12(f,n,x,y,h,yh,dy);
	s=0;
	for(i=0;i<n;i++){
		s+=dy[i]*dy[i]; 
		err=sqrt(s);
	}
	s=0;
	for(i=0;i<n;i++){
		s+=yh[i]*yh[i];
		normy=sqrt(s);
	}
	tol=(normy*eps+acc)*sqrt(h/(b-a));
	k++;
	if(err<tol){ /* accept step and continue */
		if(k>max-1){
			printf("max steps reached in ode_driver\n");
			break;
		}
		xlist[0]=x+h;
		for(i=0;i<n;i++){
			ylist[0][i]=yh[i];
		}
	}
	if(err>0){
		h*=pow(tol/err,0.25)*0.95;
	}
	else{
		h*=2;
	}
} /* end while */


	printf("\n antal kald %i\n",k);

 /* return the number of entries in xlist/ylist */
} 

int ode_driverB(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max){
int i,k=0; 
double x,*y,s,err,normy,tol,a=xlist[0],yh[n],dy[n];
while(xlist[k]<b){
	x=xlist[k], y=ylist[k]; 
	if(x+h>b){
		h=b-x;
	}
	rkstep12(f,n,x,y,h,yh,dy);
	s=0;
	for(i=0;i<n;i++){
		s+=dy[i]*dy[i]; 
		err=sqrt(s);
	}
	s=0;
	for(i=0;i<n;i++){
		s+=yh[i]*yh[i];
		normy=sqrt(s);
	}
	tol=(normy*eps+acc)*sqrt(h/(b-a));
	if(err<tol){ /* accept step and continue */
		k++;
		if(k>max-1){
			return -k; /* uups */
		}
		xlist[k]=x+h;
		for(i=0;i<n;i++){
			ylist[k][i]=yh[i];
		}
	}
	if(err>0){
		h*=pow(tol/err,0.25)*0.95;
	}
	else{
		h*=2;
	}
} /* end while */





return k+1; /* return the number of entries in xlist/ylist */
} 

void rkstep23( void f(int n,double x,double* y,double* dydx),
int n, double x, double* yx, double h, double* yh, double* dy){
  int i; double k1[n],k2[n],k3[n],k4[n],yt[n]; /* VLA: -std=c99 */
  f(n,   x    ,yx,k1); for(i=0;i<n;i++) yt[i]=yx[i]+1./2*k1[i]*h;
  f(n,x+1./2*h,yt,k2); for(i=0;i<n;i++) yt[i]=yx[i]+3./4*k2[i]*h;
  f(n,x+3./4*h,yt,k3); for(i=0;i<n;i++)
    yh[i]=yx[i]+(2./9 *k1[i]+1./3*k2[i]+4./9*k3[i])*h;
  f(n,  x+h   ,yh,k4);  for(i=0;i<n;i++){
    yt[i]=yx[i]+(7./24*k1[i]+1./4*k2[i]+1./3*k3[i]+1./8*k4[i])*h;
    dy[i]=yh[i]-yt[i];
    }
}


void ode_driverC(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max){
int i,k=0; 
double x,*y,s,err,normy,tol,a=xlist[0],yh[n],dy[n];
while(xlist[0]<b){
	x=xlist[0], y=ylist[0]; 
	if(x+h>b){
		h=b-x;
	}
	rkstep23(f,n,x,y,h,yh,dy);
	s=0;
	for(i=0;i<n;i++){
		s+=dy[i]*dy[i]; 
		err=sqrt(s);
	}
	s=0;
	for(i=0;i<n;i++){
		s+=yh[i]*yh[i];
		normy=sqrt(s);
	}
	tol=(normy*eps+acc)*sqrt(h/(b-a));
	k++;
	if(err<tol){ /* accept step and continue */
		if(k>max-1){
			printf("max steps reached in ode_driver\n");
			break;
		}
		xlist[0]=x+h;
		for(i=0;i<n;i++){
			ylist[0][i]=yh[i];
		}
	}
	if(err>0){
		h*=pow(tol/err,0.25)*0.95;
	}
	else{
		h*=2;
	}
} /* end while */


	printf("\n antal kald %i\n",k);

 /* return the number of entries in xlist/ylist */
} 
