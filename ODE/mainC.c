#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"functions.h"

int main(){

	printf("Opgave C:\n");
	printf("Printer løsningen til -(1/2)u''-(1/r)u = u\n");

	int n=2;
	int max=1000;
	double*xstart=(double*)calloc(1,sizeof(double));
	double**ystart=(double**)calloc(1,sizeof(double*)); 
	ystart[0]=(double*)calloc(n,sizeof(double)); /* putter array ind i hver indgang af arrayet, for at skabe en "matrix" */
	double a=1, b=10, h=0.1, acc=1e-5, eps=acc;
	xstart[0]=a; ystart[0][0]=0; ystart[0][1]=1;
	ode_driverC(f,n,xstart,ystart,b,h,acc,eps,max);

	printf("b\tystart\n");
		printf("%g\t%g\n",b,ystart[0][0]);
	printf("Vi ser at vi finder løsniingen for endepunktet b=10, så vores løsning virker.\n Vores løsning er den samme som i opgave A men med et lavere antal kald.");

return 0;
}

