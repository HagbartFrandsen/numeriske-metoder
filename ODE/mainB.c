#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"functions.h"

int main(){
	printf("Opgave B:\n");
	printf("Printer løsningen til -(1/2)u''-(1/r)u = u\n");

	int n=2;
	int max=1000;
	double*xlist=(double*)calloc(max,sizeof(double));
	double**ylist=(double**)calloc(max,sizeof(double*)); 
	for(int i=0;i<max;i++){
		ylist[i]=(double*)calloc(n,sizeof(double)); /* putter array ind i hver indgang af arrayet, for at skabe en "matrix" */
	}
	double a=1, b=10, h=0.1, acc=1e-3, eps=acc;
	xlist[0]=a; ylist[0][0]=0; ylist[0][1]=1;
	int k = ode_driverB(f,n,xlist,ylist,b,h,acc,eps,max);
	if(k<0){
		printf("max steps reached in ode_driver\n");
	}
	printf("b\tystart\n\n\n");
	for(int i=0;i<k;i++){
		printf("%g\t%g\n",xlist[i],ylist[i][0]);
	}
return 0;
}

