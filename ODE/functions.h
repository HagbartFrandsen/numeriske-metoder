void rkstep12(void f(int n, double x, double*yx, double*dydx),
int n, double x, double* yx, double h, double* yh, double* dy);


void ode_driverA(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max);

void f(int n, double x, double* y, double* dydx);

int ode_driverB(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max);



void rkstep23( void f(int n,double x,double* y,double* dydx),
int n, double x, double* yx, double h, double* yh, double* dy);


void ode_driverC(void f(int n,double x,double*y,double*dydx), int n,double*xlist,double**ylist, double b,double h,double acc,double eps,int max);
