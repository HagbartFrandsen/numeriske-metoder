#include<stdio.h>
#include<stdlib.h>
#include"functions.h"



int main(){

int n=5, i; /* laver datasæt med n punkter */
double x[n], y[n], j;
for(i=0;i<n;i++){
	x[i]=i;
	y[i]=x[i]*x[i];
}

cubic_spline *s=cubic_spline_alloc(n,x,y);

double z;
for(j=0;j<n-1;j+=0.1){
z=cubic_spline_eval(s,j);
printf("%g \t %g\n",j,z);
}

printf("\n\n");

for(j=0;j<n-1;j+=0.1){
z=cubic_spline_derivative(s,j);
printf("%g \t %g\n",j,z);
}

for(j=0;j<n-1;j+=0.1){
z=cubic_spline_integral(s,j);
printf("%g \t %g\n",j,z);
}

cubic_spline_free(s);

return 0;
}
