double linterp(int n, double* x, double* y, double z);

double linterp_integ(int n, double *x, double *y, double z);

typedef struct{
int n; 
double *x, *y, *b, *c;
} qspline;
/* finder coefficienterne til vores spline */
qspline* qspline_alloc(int n, double* x, double* y);
/* evaluere vores spline */
double qspline_eval(qspline *s, double z);

double qspline_derivative(qspline *s, double z);

double qspline_integral(qspline *s, double z);
/* evaluere vores spline */
double qspline_eval(qspline *s, double z);
/* frigør hukommelsen */
void qspline_free(qspline *s);

double qspline_derivative(qspline *s, double z);

double qspline_integral(qspline *s, double z);


typedef  struct{
int n; 
double *x, *y, *b, *c, *d;} cubic_spline;
cubic_spline* cubic_spline_alloc(int n, double *x, double *y);

double cubic_spline_eval(cubic_spline *s, double z);

double cubic_spline_derivative(cubic_spline *s, double z);

double cubic_spline_integral(cubic_spline *s, double z);

void cubic_spline_free(cubic_spline *s);





