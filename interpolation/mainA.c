#include<stdio.h>
#include<stdlib.h>
#include"functions.h"


int main(){
int n=5, i; /* laver datasæt med n punkter */
double x[n], y[n], j;
for(i=0;i<n;i++){
	x[i]=i;
	y[i]=x[i];
}
for(int i=0;i<n;i++){
	printf("%g %g\n",x[i],y[i]);
}
printf("\n\n");
double lin;
for(j=0; j<=n-1; j+=0.1){ /* laver lineær interpolation for alle punkterne */
lin=linterp(n, x, y, j);
printf("%g \t %g\n",j,lin);
}
printf("\n\n");
double integ;
for(j=0; j<=n-1; j+=0.1){ /* laver lineær interpolation for alle punkterne */
integ=linterp_integ(n, x, y, j);
printf("%g \t %g\n",j,integ);
}

/* tester med en anden funktion */
printf("\n\n");
n=10; /* laver datasæt med n punkter */
double x0[n], y0[n];
for(i=0;i<n;i++){
	x0[i]=i;
	y0[i]=x0[i]*x0[i];
}
for(int i=0;i<n;i++){
	printf("%g %g\n",x0[i],y0[i]);
}
printf("\n\n");
double lin0;
for(j=0; j<=n-1; j+=0.1){ /* laver lineær interpolation for alle punkterne */
lin0=linterp(n, x0, y0, j);
printf("%g \t %g\n",j,lin0);
}
printf("\n\n");
double integ0;
for(j=0; j<=n-1; j+=0.1){ /* laver lineær interpolation for alle punkterne */
integ0=linterp_integ(n, x0, y0, j);
printf("%g \t %g\n",j,integ0);
}
return 0;
}
