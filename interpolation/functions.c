#include<assert.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

double linterp(int n, double* x, double* y, double z){
assert(n>1 && z>=x[0] && z<=x[n-1]);
int i=0, j=n-1;
while(j-i>1){
	int m=(i+j)/2;
	if(z>x[m])
	i=m;
	else
	j=m;
}
return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);
}




double linterp_integ(int n, double *x, double *y, double z){
int i=0; /* finder antal punkter i x[i] mellem x[0] og z */
while(z>x[i]){
	i++;
}
int Maxi=i-1;
double integ=0; /* arealet findes ved trapez metode, så arealet melem hver punkt ses som en rektangle */
int j;
for(j=0;j<Maxi;j++){
	integ+=y[j]*(x[j+1]-x[j])+0.5*(x[j+1]-x[j])*(y[j+1]-y[j]);
}
double lin=linterp(n, x, y, z); /* finder det sidste stykke areal mellem x[i] og z */
integ+=y[Maxi]*(z-x[Maxi])+0.5*(z-x[Maxi])*(lin-y[Maxi]);
return integ;
}

typedef struct{
int n; 
double *x, *y, *b, *c;
} qspline;

/* finder coefficienterne til vores spline */
qspline* qspline_alloc(int n, double* x, double* y){ /* bygger spline */
qspline *s=(qspline*)malloc(sizeof(qspline)); /* spline */
s->b=(double*)malloc((n-1)*sizeof(double)); /* bi */
s->c=(double*)malloc((n-1)*sizeof(double)); /* ci */
s->x=(double*)malloc(n*sizeof(double)); /* xi */
s->y=(double*)malloc(n*sizeof(double)); /* yi */
s->n=n; 
for(int i=0; i<n; i++){
	s->x[i]=x[i];
	s->y[i]=y[i];
}

int i;
double p[n-1], h[n-1];
for(int i=0; i<n-1; i++){
	h[i]=x[i+1]-x[i];
	p[i]=(y[i+1]-y[i])/h[i];
}

s->c[0]=0;

for(int i=0; i<n-2; i++){
s->c[i+1]=(p[i+1]-p[i]-s->c[i]*h[i])/h[i+1];
}

s->c[n-2]/=2;

for(i=n-3; i>=0; i--){
s->c[i]=(p[i+1]-p[i]-s->c[i+1]*h[i+1])/h[i];
}

for(i=0;i<n-1;i++){
s->b[i]=p[i]-s->c[i]*h[i];
}
return s;
}

/* evaluere vores spline */
double qspline_eval(qspline *s, double z){
assert(z>=s->x[0] && z<=s->x[s->n-1]);
int i=0, j=s->n-1;
while(j-i>1){
/* binary søgen */
int m=(i+j)/2;
if(z>s->x[m]) 
i=m; 
else 
j=m;
}
double h=z-s->x[i];
return s->y[i]+h*(s->b[i]+h*s->c[i]);
}

/* frigør hukommelsen */
void qspline_free(qspline *s){
free(s->x);
free(s->y);
free(s->b);
free(s->c);
free(s);
}



double qspline_derivative(qspline *s, double z){ /* evaluates the derivative of the spline at point z */
	double *x=s->x;
	if(z<x[0] || z>x[s->n-1]) printf("qseval: out of range: z=%g",z);
	int i=0, j=s->n-1;
	while(j-i>1){int m=(i+j)/2; if(z>x[m]) i=m; else j=m;} //bisection
	double h=z-x[i];
	return s->b[i]+2*h*s->c[i]; //derivative
}// end qspline_deriv



double qspline_integral(qspline *s, double z){
	double *x=s->x;
	assert(z>=s->x[0] && z<=s->x[s->n-1]);
	int i=0, j=s->n-1;
	
while(j-i>1){
	int m=(i+j)/2; 
	if(z>x[m]) 
	i=m; 
	else 
	j=m;
}


double integ=0, h1,h2;
for(int l=0;l<=i-1;l++){
h1=s->x[l+1]-s->x[l];
integ+=h1*s->y[l]+0.5*s->b[l]*h1*h1+s->c[l]*h1*h1*h1/3;
}

h2=z-s->x[i];
integ+=h2*s->y[i]+0.5*s->b[i]*h2*h2+s->c[i]*h2*h2*h2/3;


return integ;
}

typedef  struct{
int n; 
double *x, *y, *b, *c, *d;} cubic_spline;
cubic_spline* cubic_spline_alloc(int n, double *x, double *y)
{
//   b u i l d s   n a t u r a l   c u b i c   s p l i n e
cubic_spline* s=(cubic_spline*)malloc(sizeof(cubic_spline));/* spline */
s->x=(double*)malloc(n*sizeof(double)); /* xi */
s->y=(double*)malloc(n*sizeof(double)); /* yi */
s->b=(double*)malloc(n*sizeof(double)); /* bi */
s->c=(double*)malloc((n-1)*sizeof(double)); /* ci */
s->d=(double*)malloc((n-1)*sizeof(double)); /* di */
s->n=n;
for(int i=0; i<n; i++){
s->x[i]=x[i];
s->y[i]=y[i];
}
double h[n-1], p[n-1];
//  VLA
for(int i=0; i<n-1; i++){
h[i]=x[i+1]-x[i];
assert(h[i]>0);
}
for(int i =0; i<n-1; i++){
p[i]=(y[i+1]-y[i])/h[i];
}
double D[n], Q[n-1], B[n];
//   b u i l d i n g   t h e   t r i d i a g o n a l   s y s t e m :
D[0]=2;
for(int i=0; i<n-2; i++){
D[i+1]=2*h[i]/h[i+1]+2;
}
D[n-1]=2;
Q[0]=1;
for(int i=0; i<n-2; i++){
Q[i+1]=h[i]/h[i+1];
}
for(int i=0; i<n-2; i++){
B[i+1]=3*(p[i]+p[i+1]*h[i]/h[i+1]);
}
B[0]=3*p[0];
B[n-1]=3*p[n-2];
// Gauss   e l i m i n a t i o n   :
for(int i=1; i<n; i++){
D[i]-=Q[i-1]/D[i-1];
B[i]-=B[i-1]/D[i-1];
}
s->b[n-1]=B[n-1]/D[n-1];
// back−s u b s t i t u t i o n   :
for(int i=n-2; i>=0; i--){
s->b[i]=(B[i]-Q[i]*s->b[i+1])/D[i];
}
for(int i=0; i<n-1; i++){
s->c[i]=(-2*s->b[i]-s->b[i+1]+3*p[i])/h[i];
s->d[i]=(s->b[i]+s->b[i+1]-2*p[i])/h[i]/h[i];
}
return s;
}

double cubic_spline_eval(cubic_spline *s, double z){
assert(z>=s->x[0] && z<=s->x[s->n-1]);
int i=0, j=s->n-1;
//  b i n a r y   s e a r c h   f o r   t h e   i n t e r v a l   f o r   z   :
while(j-i>1){
int m=(i+j)/2;
if(z>s->x[m])
i=m;
else
j=m;
}
double h=z-s->x[i];
//   c a l c u l a t e   t h e   i n e r p o l a t i n g   s p l i n e   :
return s->y[i]+h*(s->b[i]+h*(s->c[i]+h*s->d[i]));
}









double cubic_spline_derivative(cubic_spline *s, double z){ /* evaluates the derivative of the spline at point z */
	double *x=s->x;
	if(z<x[0] || z>x[s->n-1]) printf("qseval: out of range: z=%g",z);
	int i=0, j=s->n-1;
	while(j-i>1){int m=(i+j)/2; if(z>x[m]) i=m; else j=m;} //bisection
	double h=z-x[i];
	return s->b[i]+2*h*s->c[i]+3*h*h*s->d[i]; //derivative
}// end qspline_deriv



double cubic_spline_integral(cubic_spline *s, double z){ /* evaluates the integral of the spline at point z */
	double *x=s->x;
	assert(z>=s->x[0] && z<=s->x[s->n-1]);
	int i=0, j=s->n-1;
	
while(j-i>1){
	int m=(i+j)/2; 
	if(z>x[m]) 
	i=m; 
	else 
	j=m;
}


double integ=0, h1,h2;
for(int l=0;l<=i-1;l++){
h1=s->x[l+1]-s->x[l];
integ+=h1*s->y[l]+0.5*s->b[l]*h1*h1+s->c[l]*h1*h1*h1/3;
}

h2=z-s->x[i];
integ+=h2*s->y[i]+0.5*s->b[i]*h2*h2+s->c[i]*h2*h2*h2/3;


return integ;
}


void cubic_spline_free(cubic_spline *s){
// f r e e   t h e   a l l o c a t e d  memory
free(s->x); 
free(s->y); 
free(s->b); 
free(s->c);
free(s->d);
free(s);
}


