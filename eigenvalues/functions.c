#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<assert.h>

void printm(gsl_matrix *A){
	for(int r=0;r<A->size1;r++){
		for(int c=0;c<A->size2;c++) printf("%1.3g   \t",gsl_matrix_get(A,r,c));
		printf("\n");
	}
}

void printv(gsl_vector *b){
	for(int r=0;r<b->size;r++){
		printf("%1.3g\t",gsl_vector_get(b,r));
		printf("\n");
	}
}

int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
/* Jacobi diagonalization; upper triangle of A is destroyed;
   e and V accumulate eigenvalues and eigenvectors */
int changed, sweeps=0, n=A->size1;
for(int i=0;i<n;i++)gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
gsl_matrix_set_identity(V);
do{ changed=0; sweeps++; int p,q;
	for(p=0;p<n;p++)for(q=p+1;q<n;q++){
		double app=gsl_vector_get(e,p);
		double aqq=gsl_vector_get(e,q);
		double apq=gsl_matrix_get(A,p,q);
		double phi=0.5*atan2(2*apq,aqq-app);
		double c = cos(phi);
		double s = sin(phi);
		double app1=c*c*app-2*s*c*apq+s*s*aqq;
		double aqq1=s*s*app+2*s*c*apq+c*c*aqq;
		if(app1!=app || aqq1!=aqq){ changed=1;
			gsl_vector_set(e,p,app1);
			gsl_vector_set(e,q,aqq1);
			gsl_matrix_set(A,p,q,0.0);
			for(int i=0;i<p;i++){
				double aip=gsl_matrix_get(A,i,p);
				double aiq=gsl_matrix_get(A,i,q);
				gsl_matrix_set(A,i,p,c*aip-s*aiq);
				gsl_matrix_set(A,i,q,c*aiq+s*aip); }
			for(int i=p+1;i<q;i++){
				double api=gsl_matrix_get(A,p,i);
				double aiq=gsl_matrix_get(A,i,q);
				gsl_matrix_set(A,p,i,c*api-s*aiq);
				gsl_matrix_set(A,i,q,c*aiq+s*api); }
			for(int i=q+1;i<n;i++){
				double api=gsl_matrix_get(A,p,i);
				double aqi=gsl_matrix_get(A,q,i);
				gsl_matrix_set(A,p,i,c*api-s*aqi);
				gsl_matrix_set(A,q,i,c*aqi+s*api); }
			for(int i=0;i<n;i++){
				double vip=gsl_matrix_get(V,i,p);
				double viq=gsl_matrix_get(V,i,q);
				gsl_matrix_set(V,i,p,c*vip-s*viq);
				gsl_matrix_set(V,i,q,c*viq+s*vip); }
			} } }while(changed!=0);
return sweeps; }

int jacobi_eigen_by_eigen(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int row){
/* Jacobi diagonalization; upper triangle of A is destroyed;
   e and V accumulate eigenvalues and eigenvectors */
int changed, sweeps=0, n=A->size1;
for(int i=0;i<n;i++)if(i==row)gsl_vector_set(e,i,gsl_matrix_get(A,i,i));else gsl_vector_set(e,i,0);
gsl_matrix_set_identity(V);
do{ changed=0; sweeps++; int p,q;
	for(p=0;p<n;p++)for(q=p+1;q<n;q++){
		double app=gsl_vector_get(e,p);
		double aqq=gsl_vector_get(e,q);
		double apq=gsl_matrix_get(A,p,row);
		double phi=0.5*atan2(2*apq,aqq-app);
		double c = cos(phi);
		double s = sin(phi);
		double app1=c*c*app-2*s*c*apq+s*s*aqq;
		double aqq1=s*s*app+2*s*c*apq+c*c*aqq;
		if(app1!=app || aqq1!=aqq){ changed=1;
			gsl_vector_set(e,p,app1);
			gsl_vector_set(e,q,aqq1);
			gsl_matrix_set(A,p,row,0.0);
			for(int i=0;i<p;i++){
				double aip=gsl_matrix_get(A,i,row);
				double aiq=gsl_matrix_get(A,i,row);
				gsl_matrix_set(A,i,row,c*aip-s*aiq);
				gsl_matrix_set(A,i,row,c*aiq+s*aip); }
			for(int i=p+1;i<q;i++){
				double api=gsl_matrix_get(A,p,row);
				double aiq=gsl_matrix_get(A,i,row);
				gsl_matrix_set(A,p,row,c*api-s*aiq);
				gsl_matrix_set(A,i,row,c*aiq+s*api); }
			for(int i=q+1;i<n;i++){
				double api=gsl_matrix_get(A,p,row);
				double aqi=gsl_matrix_get(A,q,row);
				gsl_matrix_set(A,p,row,c*api-s*aqi);
				gsl_matrix_set(A,q,row,c*aqi+s*api); }
			for(int i=0;i<n;i++){
				double vip=gsl_matrix_get(V,i,row);
				double viq=gsl_matrix_get(V,i,row);
				gsl_matrix_set(V,i,row,c*vip-s*viq);
				gsl_matrix_set(V,i,row,c*viq+s*vip); }
			} } }while(changed!=0);
return sweeps; }


int jacobi_row(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int row){
/* Jacobi diagonalization; upper triangle of A is destroyed;
   e and V accumulate eigenvalues and eigenvectors */
int changed, sweeps=0, n=A->size1;
if(n<row) row=n-1; /* safety if row is higher than matrix dimension */
for(int i=0;i<n;i++)gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
gsl_matrix_set_identity(V);
do{ changed=0; sweeps++; int p,q;
	for(p=0;p<row+1;p++)for(q=p+1;q<n;q++){
		double app=gsl_vector_get(e,p);
		double aqq=gsl_vector_get(e,q);
		double apq=gsl_matrix_get(A,p,q);
		double phi=0.5*atan2(2*apq,aqq-app);
		double c = cos(phi);
		double s = sin(phi);
		double app1=c*c*app-2*s*c*apq+s*s*aqq;
		double aqq1=s*s*app+2*s*c*apq+c*c*aqq;
		if(app1!=app){ 
			changed=1;
			gsl_vector_set(e,p,app1);
			gsl_vector_set(e,q,aqq1);
			gsl_matrix_set(A,p,q,0.0);
			for(int i=0;i<p;i++){
				double aip=gsl_matrix_get(A,i,p);
				double aiq=gsl_matrix_get(A,i,q);
				gsl_matrix_set(A,i,p,c*aip-s*aiq);
				gsl_matrix_set(A,i,q,c*aiq+s*aip); }
			for(int i=p+1;i<q;i++){
				double api=gsl_matrix_get(A,p,i);
				double aiq=gsl_matrix_get(A,i,q);
				gsl_matrix_set(A,p,i,c*api-s*aiq);
				gsl_matrix_set(A,i,q,c*aiq+s*api); }
			for(int i=q+1;i<n;i++){
				double api=gsl_matrix_get(A,p,i);
				double aqi=gsl_matrix_get(A,q,i);
				gsl_matrix_set(A,p,i,c*api-s*aqi);
				gsl_matrix_set(A,q,i,c*aqi+s*api); }
			for(int i=0;i<n;i++){
				double vip=gsl_matrix_get(V,i,p);
				double viq=gsl_matrix_get(V,i,q);
				gsl_matrix_set(V,i,p,c*vip-s*viq);
				gsl_matrix_set(V,i,q,c*viq+s*vip); }
			} } }while(changed!=0);
return sweeps; }

/* opgave C */

int IndexOffDiagMax(gsl_matrix* A, int row){
  	int p = row+1;
  	double maxVal = fabs(gsl_matrix_get(A,row,p));
  	for(int j = p+1; j<A->size1; j++){
    		double tmp = abs(gsl_matrix_get(A,row,j));
    		if (tmp > maxVal){
      			p = j;
      			maxVal = tmp;
    		}
  	}
return p;
}

int RowOffDiagMax(gsl_matrix* A, int *index){
  	int n = A->size1,p=0;
  	double maxVal = abs(gsl_matrix_get(A,p,index[p]));
  	for(int row = p+1; row < n-1; row++){
    		if(abs(gsl_matrix_get(A,row,index[row])) > maxVal){
			p = row;
			maxVal = abs(gsl_matrix_get(A,p,index[p]));
      		}
  	}
return p;
}

int JacobiClassic(gsl_matrix* A, gsl_matrix* V, gsl_vector* e){
  	int n = A->size1;
  	assert(n>=2);
  	for(int i = 0; i<n;i++){
		gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
	}
    	gsl_matrix_set_identity(V);
  	int index[n-1];
  		for(int i =0; i < n-1;i++){
			index[i] = IndexOffDiagMax(A,i);
		}
  		int rotations = 0,changed;
  		double App, Aqq, Apq; //The elements used to calculate phi
  		double App_primed, Aqq_primed;
  		double Aip, Aiq, Api, Aqi;
  		double Vip, Viq;
  		double phi,c, s;//The constants to calculate new A elements
  		do{
    			rotations++;
    			changed = 0;
    			int p = RowOffDiagMax(A,index), q = index[p];
    			App = gsl_vector_get(e,p);
    			Aqq = gsl_vector_get(e,q);
    			Apq = gsl_matrix_get(A,p,q);
    			phi = 0.5*atan2(2*Apq,(Aqq-App));
    			c = cos(phi);
    			s = sin(phi);
    			App_primed = c*c*App-2*s*c*Apq + s*s*Aqq;
    			Aqq_primed = s*s*App+2*s*c*Apq+c*c*Aqq;
    			if ( App != App_primed  || Aqq != Aqq_primed){
    				changed = 1;
      				rotations++;
      				gsl_vector_set(e,p,App_primed);
      				gsl_vector_set(e,q,Aqq_primed);
      				gsl_matrix_set(A,p,q,0.0);
      				for(int i = 0; i < p; i++){
					Aip = gsl_matrix_get(A,i,p);
					Aiq = gsl_matrix_get(A,i,q);
					gsl_matrix_set(A,i,p,c*Aip-s*Aiq);
					gsl_matrix_set(A,i,q,c*Aiq+s*Aip);
      				}
      				for(int i = p+1;i < q; i++){
					Api = gsl_matrix_get(A,p,i);
					Aiq = gsl_matrix_get(A,i,q);
					gsl_matrix_set(A,p,i,c*Api-s*Aiq);
					gsl_matrix_set(A,i,q,c*Aiq+s*Api);
      				}
      				for(int i = q+1; i < n; i++){
					Api = gsl_matrix_get(A,p,i);
					Aqi = gsl_matrix_get(A,q,i);
					gsl_matrix_set(A,p,i,c*Api-s*Aqi);
					gsl_matrix_set(A,q,i,c*Aqi+s*Api);
      				}
      				for(int i =0; i<n;i++){
					Vip = gsl_matrix_get(V,i,p);
					Viq = gsl_matrix_get(V,i,q);
					gsl_matrix_set(V,i,p,c*Vip-s*Viq);
					gsl_matrix_set(V,i,q,c*Viq+s*Vip);
      				} 

    			}
    			index[p] = IndexOffDiagMax(A,p);
    			if(q<n-1) index[q] = IndexOffDiagMax(A,q);
    			for(int i = 0; i<p;i++){
      				if(index[i] == p || index[i] == q){
					index[i] = IndexOffDiagMax(A,i);
				} else{
	if(abs(gsl_matrix_get(A,i,p)) > abs(gsl_matrix_get(A,i,index[i]))) index[i]=p;
	if(abs(gsl_matrix_get(A,i,q)) > abs(gsl_matrix_get(A,i,index[i]))) index[i]=q;
      				}
    			}
    			for(int i = p+1;i<q;i++){
      				if(index[i]==q) index[i]=IndexOffDiagMax(A,i);
    	else if(abs(gsl_matrix_get(A,i,q)) > abs(gsl_matrix_get(A,i,index[i]))) index[i] = q;
    			}
  		}while(changed);

return rotations;
}

