#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<stdlib.h>
#include"functions.h"

int main(){
printf("Opgave B.a\n");
printf("laver først en symmetrisk matriks A\n");
size_t n=4;
gsl_matrix* A = gsl_matrix_alloc(n,n);
for(int i=0;i<n;i++){
	for(int j=i;j<n;j++){
		gsl_matrix_set(A,i,j,((double)rand()/RAND_MAX));
		if(i!=j){
			gsl_matrix_set(A,j,i,gsl_matrix_get(A,i,j));
		}	
	}
}

printm(A);

gsl_matrix* Acopy = gsl_matrix_alloc(n,n);
gsl_matrix_memcpy(Acopy,A);

gsl_matrix* V = gsl_matrix_alloc(n,n);
gsl_vector* e = gsl_vector_alloc(n);
for(int i=0;i<n;i++){
	gsl_vector_set(e,i,((double)rand()/RAND_MAX));
}

gsl_vector* ecopy = gsl_vector_alloc(n);
gsl_vector_memcpy(ecopy,e);

int row=0;
int sweeps=jacobi_row(A, e, V, row);
printf("printer A efter jacobi\n");
printm(A);
printf("printer V efter jacobi\n");
printm(V);
printf("printer e efter jacobi\n");
printv(e);

printf("n=%i, sweeps=%i\n",n,sweeps);

printf("printer V^TAV, og ser om den første diagonal indgang er den første egenværdi fra opgave A\n");


gsl_matrix* VT = gsl_matrix_alloc(n,n); 
gsl_matrix_transpose_memcpy(VT,V);

gsl_matrix* VTA = gsl_matrix_calloc(n,n);
gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, VT, Acopy, 0.0, VTA); 
gsl_matrix* VTAV = gsl_matrix_calloc(n,n);
gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, VTA, V, 0.0, VTAV); 
printm(VTAV);

printf("Opgave B.b\n");
printf("laver først en symmetrisk matriks A\n");
gsl_matrix_memcpy(A,Acopy);

printm(A);

V = gsl_matrix_alloc(n,n);
gsl_vector_memcpy(e,ecopy);



row=1;
sweeps=jacobi_row(A, e, V, row);
printf("printer A efter jacobi\n");
printm(A);
printf("printer V efter jacobi\n");
printm(V);
printf("printer e efter jacobi\n");
printv(e);

printf("n=%i, sweeps=%i\n",n,sweeps);

printf("printer V^TAV, og ser om de første to diagonal indgang er den første to egenværdier fra opgave A\n");

gsl_matrix_transpose_memcpy(VT,V);

gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, VT, Acopy, 0.0, VTA); 
gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, VTA, V, 0.0, VTAV); 
printm(VTAV);

printf("Opgave B.c\n");
printf("laver først en symmetrisk matriks A\n");
gsl_matrix_memcpy(A,Acopy);

printm(A);

V = gsl_matrix_alloc(n,n);
gsl_vector_memcpy(e,ecopy);

row=41563565;
sweeps=jacobi_row(A, e, V, row);
printf("printer A efter jacobi\n");
printm(A);
printf("printer V efter jacobi\n");
printm(V);
printf("printer e efter jacobi\n");
printv(e);

printf("n=%i, sweeps=%i\n",n,sweeps);

printf("printer V^TAV, og ser om alle diagonal indgange de samme egenværdier fra opgave A\n");

gsl_matrix_transpose_memcpy(VT,V);

gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, VT, Acopy, 0.0, VTA); 
gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, VTA, V, 0.0, VTAV); 
printm(VTAV);

gsl_matrix_free(A);
gsl_matrix_free(Acopy);
gsl_matrix_free(V);
gsl_matrix_free(VT);
gsl_matrix_free(VTA);
gsl_matrix_free(VTAV);
gsl_vector_free(e);
gsl_vector_free(ecopy);

return 0;
}
