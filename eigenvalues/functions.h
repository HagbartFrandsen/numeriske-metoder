#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>

void printm(gsl_matrix *A);
void printv(gsl_vector *b);

int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

int jacobi_eigen_by_eigen(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int row);

int jacobi_row(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int row);

int jacobi_classic_algorithm(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

int JacobiClassic(gsl_matrix* A, gsl_matrix* V, gsl_vector* e);
